<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get( '/','HomeController@index')->name('index');
Route::get( '/digital','HomeController@digital')->name('digital');
Route::match(['get', 'post'], '/contact',	'HomeController@contact')->name('contact');
Route::match(['get', 'post'], '/contactus',	'HomeController@contactUSPost')->name('contactus');
Route::match(['get', 'post'], '/trace',	'HomeController@trace')->name('trace');
Route::get('/packages' ,'HomeController@package')->name('packages');
Route::get('/packages/web' ,'HomeController@package_web')->name('packages_web');
Route::get('/package/{id}' ,'HomeController@package_detail')->name('packages_detail');
Route::get('/package/web/{id}' ,'HomeController@package_web_detail')->name('packages_web_detail');
Route::get('/about' ,'HomeController@about')->name('about');
Route::get('/privacy-policy' ,'HomeController@privacypolicy')->name('privacypolicy');
Route::get('/terms' ,'HomeController@terms')->name('terms');


// Route::get('/home', 'HomeController@index')->name('home');
// Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
// Route::post('login', 'Auth\LoginController@login');	
// Route::post('logout', 'Auth\LoginController@logout')->name('logout');



Route::namespace('Admin')->prefix(env('ADMIN_PREFIX', 'admin'))->group(function(){

	Route::get('login', 'Auth\LoginController@showLoginForm')->name('admin.login');
	Route::post('login', 'Auth\LoginController@login');	


	Route::middleware('auth:admin')->group(function(){
		Route::get('/', 'AdminController@index')->name('admin.home');
		Route::post('logout', 'Auth\LoginController@logout')->name('admin.logout');

		Route::match(['get', 'post'], '/googleanalytics','AdminController@googleanalytics')->name('admin.googleanalytics');

		//Profile
		Route::get('/profile/{id}', 'AdminController@profile')->name('admin.profile');
		Route::post('/profile/setting', 'AdminController@setting')->name('admin.setting');
		Route::post('/profile/password', 'AdminController@password')->name('admin.password');

		//Slider
		Route::match(['get', 'post'], 'slide',	'SlideController@index')->name('slide.index');
		Route::post('slide/add',				'SlideController@store')->name('slide.store');
		Route::put('slide/update/{id}',			'SlideController@update')->name('slide.update');
		Route::delete('slide/delete/{id}', 		'SlideController@destroy')->name('slide.destroy');

		//Package
		Route::match(['get', 'post'],'package',	'PackageController@index')->name('package.index');
		Route::match(['get', 'post'],'package/add',	'PackageController@store')->name('package.store');
		Route::match(['get', 'post'],'package/update/{id}',	'PackageController@update')->name('package.update');
		Route::post('package/delete/{id}', 		'PackageController@destroy')->name('package.destroy');

		//Digital Imaging
		Route::match(['get', 'post'], 'digital',	'DigitalController@index')->name('digital.index');

		//Tracking
		Route::match(['get', 'post'], 'tracking',	'TrackingController@index')->name('tracking.index');
		Route::post('tracking/add',				'TrackingController@store')->name('tracking.store');
		Route::put('tracking/update/{id}',			'TrackingController@update')->name('tracking.update');
		Route::post('tracking/change',			'TrackingController@change')->name('tracking.change');
		Route::post('tracking/delete/{id}', 		'TrackingController@destroy')->name('tracking.destroy');
		Route::match(['get', 'post'], 'tracking/cancel',	'TrackingController@cancel')->name('tracking.cancel');

		//Contact
		Route::match(['get', 'post'], 'contact',	'ContactController@index')->name('contact.index');

		//Package_web
		Route::match(['get', 'post'],'package/web',	'PackageWebController@index')->name('packageweb.index');
		Route::match(['get', 'post'],'package/web/add',	'PackageWebController@store')->name('packageweb.store');
		Route::match(['get', 'post'],'package/web/update/{id}',	'PackageWebController@update')->name('packageweb.update');
		Route::post('package/web/delete/{id}', 		'PackageWebController@destroy')->name('packageweb.destroy');

		//About US
		Route::match(['get', 'post'], 'about',	'AboutController@index')->name('about.index');

		//Migrate
		Route::get('/migrate', function () {
			$stream = fopen('php://output', 'w');
   			 $exitCode = Artisan::call('migrate', [], new \Symfony\Component\Console\Output\StreamOutput($stream));

   		});

	});

});