const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */



mix
.copy('node_modules/jquery/dist/jquery.min.js', 'public/js/jquery.min.js')
.copy('node_modules/bootstrap/dist/js/bootstrap.min.js', 'public/js/bootstrap.min.js')
.copy('node_modules/admin-lte/dist/js/adminlte.min.js', 'public/js/adminlte.min.js')
.copy('node_modules/datatables.net/js/jquery.dataTables.js', 'public/js/jquery.dataTables.js')
.copy('node_modules/datatables.net-bs/js/dataTables.bootstrap.js', 'public/js/dataTables.bootstrap.js')
.copy('node_modules/toastr/build/toastr.min.js', 'public/js/toastr.min.js')
.copy('node_modules/jquery.easing/jquery.easing.min.js', 'public/js/jquery.easing.min.js')
.copy('node_modules/jquery.stellar/jquery.stellar.js', 'public/js/jquery.stellar.js')
.copy('node_modules/owl.carousel/dist/owl.carousel.min.js', 'public/js/owl.carousel.min.js')
.copy('node_modules/flexslider/jquery.flexslider-min.js', 'public/js/jquery.flexslider-min.js')
.copy('node_modules/jquery-countto/jquery.countTo.js', 'public/js/jquery.countTo.js')
.copy('node_modules/jquery-tags-input/dist/jquery.tagsinput.min.js', 'public/js/jquery.tagsinput.min.js')
.copy('node_modules/magnific-popup/dist/jquery.magnific-popup.min.js', 'public/js/jquery.magnific-popup.min.js');

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
   .sass('resources/sass/bootstrap.scss', 'public/css');


mix.sass('resources/sass/auth.scss', 'public/css')
	.js('resources/js/auth.js', 'public/js');

mix.sass('resources/sass/main.scss', 'public/css')	
	.js('resources/js/main.js', 'public/js');   

if (mix.inProduction() || process.env.npm_lifecycle_event !== 'hot') {
   mix.version();
}