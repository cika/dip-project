@extends('layouts.admin')
@section('content-header')
<h1>
   Add Package
</h1>
<ol class="breadcrumb">
    <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/admin/package">Package</a></li>
    <li class="active"><a href="#"> Add Package</a></li>
</ol>
@endsection
@section('content')
<div class="box">
   <div class="box-header with-border">
     <form method="post" enctype="multipart/form-data" id ="FormPackage">
            <div class="panel panel-primary" id="uploadImage">
    		        <div class="panel-heading">Images</div>
    		        <div class="panel-body">
    		          <a href="#" class="btn btn-sm btn-primary" id="btnAddImage"><i class="fa fa-plus"></i> Add Image</a>
    		          <div class="row" id="imageUpload"></div>
    		        </div>
    		    </div>
            <div class="form-group">
               <label>Title</label>
               <input type="text" class="form-control" name="title" id="title" placeholder="Masukkan Judul Package" required>
            </div>
            <div class="form-group">
               <label>Price</label>
               <div class="input-group">
                  <span class="input-group-addon">Rp.</span>
                  <input type="number" class="form-control" name="price" id="price" placeholder="Massukan Harga Package" required>
               </div>
            </div>
            <div class="form-group">
               <label>Unit</label>
               <select name="unit" class="form-control select2" style="width: 100%;" required>
                  <option disabled="disabled" selected="selected">Please choose a Status ...</option>
                  <option value="SKU">SKU</option>
                  <option value="Frame">Frame</option>
                  <option value="Banner">Banner</option>
               </select>
            </div>
            <div class="form-group">
               <label>Type</label>
               <input type="text" class="form-control" name="type" id="type" placeholder="Masukkan Type Package">
            </div>		
            <div class="form-group">
               <label>Frame</label>
               <input type="number" class="form-control" name="frame" id="frame" placeholder="Masukkan Berapa Frame Package">
            </div>
            <div class="form-group">
               <label>Detail</label>
               <textarea id="detail" name="detail" class="form-control" required></textarea>
            </div>
            <div class="form-group">
               <label>Desciption</label>
               <textarea id="description" name="description" class="form-control" required></textarea>
            </div>
             <button type="submit" id="submit-all" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-spin'></i>">Save</button>
     </form>
 </div>
 
@endsection
@section('js')
<script>
    jQuery(document).ready(function() {
       var image_id = 0;
       var data = [];

        $(document).on('click', '#btnAddImage', function (event) {
            event.preventDefault();
            var input = $('<input type="file" name="images[]" class="images-item" data-id="image-'+ image_id+'" style="display:none;" accept="image/x-png,image/gif,image/jpeg"/>');
            $('#FormPackage').append(input);
            input.click();
            image_id++;
        });

        $(document).on('change', '#FormPackage input.images-item', function(event) {
          event.preventDefault();
          var preview = $('<img class="image-preview" data-id="'+ $(this).data('id') +'"/>');
          readURL(this, preview)
          $('#imageUpload').append($('<div class="col-md-4"></div>').append(preview).append('<div style="text-align:center;"><button class="btn deleteBtn">hapus</button></div>').append(this));
        });
        
        $(document).on('click', '.deleteBtn', function(event){
          event.preventDefault();
          $(this).parent().parent().remove();
        
        });

        $('#FormPackage').submit(function (event) {
            event.preventDefault();
            $('#error').hide();
            var _button = $('#FormPackage button[type=submit]');
            _button.button('loading');
            var data = new FormData($(this)[0]);
            $.ajax({
                url: '/admin/package/add',
                type: 'POST',
                data: data,
                cache: false,
                processData: false, 
                contentType: false,
                success: function (response) {
                  console.log(data);
                    if (response.success) {
                        _button.button('reset');
                        $('#FormPackage')[0].reset();
                        toastr.success(response.message);
                        setTimeout(function () { 
                           location.replace('/admin/package');
                        }, 1000);
                    }
                    else{
                        toastr.error(response.message);
                        _button.button('reset');
                    }
                },
             	error: function(response) {
                    if(response.status === 422){
                     let errors = response.responseJSON.errors;
                     $.each(errors, function(key, error){
                       var item = form.find('input[name='+ key +']');
                       item = (item.length > 0) ? item : form.find('select[name='+ key +']');
                       item = (item.length > 0) ? item : form.find('textarea[name='+ key +']');
                       item = (item.length > 0) ? item : form.find("input[name='"+ key +"[]']");

                      var parent = (item.parent().hasClass('form-group')) ? item.parent() : item.parent().parent();
                       parent.addClass('has-error');
                       parent.append('<span class="help-block">'+ error +'</span>');
                       _button.button('reset');
                     })
                   }
                   _button.button('reset');
                 }
            });
        });
        
        $( '.btnDiscard' ).on('click', function () {
            $('#inputImage').val('');
            $('#image-preview').attr('src', '')
            $('#addImageModal').modal('hide');
        });

        function readURL(input, image) {
    	    if (input.files && input.files[0]) {
    	        var reader = new FileReader();
    	        reader.onload = function (e) {
    	            image.attr('src', e.target.result)
    	        };
    	        reader.readAsDataURL(input.files[0]);
    	    }
    	}
    });
</script>
@endsection