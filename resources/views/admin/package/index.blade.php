@extends('layouts.admin')
@section('content-header')
<h1>
   Package
</h1>
<ol class="breadcrumb">
   <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
   <li class="active"><a href="#">Package</a></li>
</ol>
@endsection
@section('content')
<div class="box box-success">
   <div class="box-header with-border">
      <a href="{{ route('package.store') }}" id="btnAdd" class="pull-right btn btn-primary"><i class="fa fa-plus"></i> Add Package</a>
   </div>
   <div class="box-body">
      <div class="table-responsive">
         <table id="package_table" class="table table-striped table-bordered table-hover nowrap dataTable">
            <thead>
               <tr>
                  <th>No</th>
                  <th>Title</th>
                  <th>Frame</th>
                  <th>Type</th>
                  <th>Price</th>
                  <th>Action</th>
               </tr>
            </thead>
         </table>
      </div>
   </div>
</div>
<section class="content-header">
   <h1>
      Package Web
   </h1>
   <ol class="breadcrumb">
      <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><a href="#">Package Web</a></li>
   </ol>
</section>
<div class="box box-success">
   <div class="box-header with-border">
      <a href="{{ route('packageweb.store') }}" id="btnAddWeb" class="pull-right btn btn-primary" style="margin-right:10px;"><i class="fa fa-plus"></i> Add Package Web</a>
   </div>
   <div class="box-body">
      <div class="table-responsive">
         <table id="package_web_table" class="table table-striped table-bordered table-hover nowrap dataTable">
            <thead>
               <tr>
                  <th>No</th>
                  <th>Title</th>
                  <th>Paket</th>
                  <th>Price</th>
                  <th>Action</th>
               </tr>
            </thead>
         </table>
      </div>
   </div>
</div>
<!-- delete -->
<div class="modal fade" tabindex="-1" role="dialog" id="deletePackageModal">
   <div class="modal-dialog">
      <div class="modal-content">
         <form action="{{ route('package.destroy', ['package' => '#']) }}" method="POST" id="FormDeletePackage">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </button>
               <h4 class="modal-title">Delete Package</h4>
            </div>
            <div class="modal-body">
               <p id="del-success">Are you sure you want to remove Package ?</p>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
               <button type="submit" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-spin'></i>">Yes
               </button>
            </div>
         </form>
      </div>
   </div>
</div>

<!-- delete Web Package -->
<div class="modal fade" tabindex="-1" role="dialog" id="deletePackageWebModal">
   <div class="modal-dialog">
      <div class="modal-content">
         <form action="{{ route('packageweb.destroy', ['package' => '#']) }}" method="POST" id="FormDeletePackageWeb">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </button>
               <h4 class="modal-title">Delete Package Web</h4>
            </div>
            <div class="modal-body">
               <p id="del-success">Are you sure you want to remove Package ?</p>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
               <button type="submit" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-spin'></i>">Yes
               </button>
            </div>
         </form>
      </div>
   </div>
</div>


@endsection
@section('js')
<script type="text/javascript">
   jQuery(document).ready(function($) {
   
       @if (session('success'))
           toastr.success("{{ session('success') }}");
       @endif
   
       @if (session('error'))
           toastr.error("{{ session('error') }}");
       @endif
   
       var table = $('#package_table').DataTable({
           "bFilter": true,
           "processing": true,
           "serverSide": true,
           "lengthChange": true,
           "ajax": {
               "url": "{{ route('package.index') }}",
               "type": "POST",
               "data" : {}
           },
           "language": {
               "emptyTable": "No data available",
           },
           "columns": [
               {
                  data: null,
                  render: function (data, type, row, meta) {
                      return meta.row + meta.settings._iDisplayStart + 1;
                  },
                  "width": "20px",
                  "orderable": false,
               },
               {
                   "data": "title",
                   "orderable": true,
               },
               {
             "data": "frame",
             "orderable": false,
         },
               {
             "data": "type",
             "orderable": false,
         },
               {
                   data:null,
                   render : function (data, type, row) {
   
                       return 'Rp. ' + parseInt(data.price).format(0, 3, '.', ',');
                   },
                   "orderable": false,
         },
               {
                   render : function(data, type, row){
                       return  '<a href="/admin/package/update/'+row.id+'" data-toggle="tooltip" title="Edit" class="edit-btn badge bg-orange"><i class="fa fa-pencil"></i></a> &nbsp;' +
                               '<a href="#" data-toggle="tooltip" title="Delete" class="delete-btn badge bg-red"><i class="fa fa-trash"></i></a>'
                               ;
                   },
                   "width": "10%",
                   "orderable": false,
               }
           ],
           "order": [ 1, 'asc' ],
           "fnCreatedRow" : function(nRow, aData, iDataIndex) {
               $(nRow).attr('data', JSON.stringify(aData));
           }
       });
   
       var table_web = $('#package_web_table').DataTable({
           "bFilter": true,
           "processing": true,
           "serverSide": true,
           "lengthChange": true,
           "ajax": {
               "url": "{{ route('packageweb.index') }}",
               "type": "POST",
               "data" : {}
           },
           "language": {
               "emptyTable": "No data available",
           },
           "columns": [
               {
                  data: null,
                  render: function (data, type, row, meta) {
                      return meta.row + meta.settings._iDisplayStart + 1;
                  },
                  "width": "20px",
                  "orderable": false,
               },
               {
                   "data": "title",
                   "orderable": true,
               },
               {
                   "data": "paket",
                   "orderable": false,
               },
               {
                   data:null,
                   render : function (data, type, row) {
   
                       return 'Rp. ' + parseInt(data.currentprice).format(0, 3, '.', ',');
                   },
                   "orderable": false,
               },
               {
                   render : function(data, type, row){
                       return  '<a href="/admin/package/web/update/'+row.id+'" data-toggle="tooltip" title="Edit" class="editweb-btn badge bg-orange"><i class="fa fa-pencil"></i></a> &nbsp;' +
                               '<a href="#" data-toggle="tooltip" title="Delete" class="deleteweb-btn badge bg-red"><i class="fa fa-trash"></i></a>'
                               ;
                   },
                   "width": "10%",
                   "orderable": false,
               }
           ],
           "order": [ 1, 'asc' ],
           "fnCreatedRow" : function(nRow, aData, iDataIndex) {
               $(nRow).attr('data', JSON.stringify(aData));
           }
       });
   
   // Delete Package
   $('#package_table').on('click', '.delete-btn' , function(e){
       var aData = JSON.parse($(this).parent().parent().attr('data'));
       url =  $('#FormDeletePackage').attr('action').replace('#', aData.id);
       $('#deletePackageModal').modal('show');
   });
   
   $('#FormDeletePackage').submit(function (event) {
       event.preventDefault();               
       $('#deletePackageModal button[type=submit]').button('loading');
       var _data = $("#FormDeletePackage").serialize();
   
       $.ajax({
           url: url,
           type: 'POST',
           data: _data,
           dataType: 'json',
           cache: false,
   
           success: function (response) {
               console.log(_data);
               if (response.success) {
                   table.draw();
                       
                   toastr.success(response.message);
                   $('#deletePackageModal').modal('toggle');
               }
               else{
                   toastr.error(response.message);
               }
               $('#deletePackageModal button[type=submit]').button('reset');
               $('#FormDeletePackage')[0].reset();
           },
           error: function(response){
               if (response.status === 400 || response.status === 422) {
                   // Bad Client Request
                   toastr.error(response.responseJSON.message);
               } else {
                   toastr.error("Whoops, looks like something went wrong.");
               }
   
               $('#FormDeletePackage button[type=submit]').button('reset');
           }
       });
   });
   
   
   
   // Delete Package Web
   $('#package_web_table').on('click', '.deleteweb-btn' , function(e){
       var aData = JSON.parse($(this).parent().parent().attr('data'));
       url =  $('#FormDeletePackageWeb').attr('action').replace('#', aData.id);
       $('#deletePackageWebModal').modal('show');
   });
   
   $('#FormDeletePackageWeb').submit(function (event) {
       event.preventDefault();               
       $('#deletePackageWebModal button[type=submit]').button('loading');
       var _data = $("#FormDeletePackageWeb").serialize();
   
       $.ajax({
           url: url,
           type: 'POST',
           data: _data,
           dataType: 'json',
           cache: false,
   
           success: function (response) {
               console.log(_data);
               if (response.success) {
                   table_web.draw();
                       
                   toastr.success(response.message);
                   $('#deletePackageWebModal').modal('toggle');
               }
               else{
                   toastr.error(response.message);
               }
               $('#deletePackageWebModal button[type=submit]').button('reset');
               $('#FormDeletePackageWeb')[0].reset();
           },
           error: function(response){
               if (response.status === 400 || response.status === 422) {
                   // Bad Client Request
                   toastr.error(response.responseJSON.message);
               } else {
                   toastr.error("Whoops, looks like something went wrong.");
               }
   
               $('#FormDeletePackageWeb button[type=submit]').button('reset');
           }
       });
   });
   
   
   
   });
</script>
@endsection