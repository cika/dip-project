<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <title>{{ config('app.name') }} | Log in Admin</title>
 <!-- Tell the browser to be responsive to screen width -->
 <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 <!-- CSRF Token -->
 <meta name="csrf-token" content="{{ csrf_token() }}">

 <!-- CSS -->
 <link rel="stylesheet" type="text/css" href="{{ mix('css/bootstrap.css') }}">
 <link rel="stylesheet" type="text/css" href="{{ mix('css/auth.css') }}">

</head>
<body class="hold-transition login-page">
   <div class="login-box">
       <div class="login-logo">
           <a href="{{ config('app.url') }}"> <b>{{ config('app.name') }}</b> admin</a>
       </div>
       <!-- /.login-logo -->

       <div class="login-box-body">
     <div class="login-box-body">
            <p class="login-box-msg">Authorized User Only</p>
            <form class="form-horizontal" method="POST" action="{{ route('admin.login') }}">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
                    <input type="text" class="form-control" name="email" placeholder="E-Mail" value="{{ old('email') }}" required autofocus>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                    
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
                    <input id="password" placeholder="password" type="password" class="form-control" name="password" required>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                            </label>
                        </div>
                   </div>
                    <div class="col-md-5">
                        <button type="submit" class="btn btn-primary btn-block btn-flat" >Sign In</button>
                    </div>
                </div>
            </form>
                
        </div>
       </div>
       <!-- /.login-box-body -->
   </div>
   <!-- /.login-box -->

<!-- JS -->
<script src="{{ asset('js//jquery.min.js') }}"></script>
<script src="{{ asset('js//bootstrap.min.js') }}"></script>
<script src="{{ asset('js//icheck.min.js') }}"></script>
<script src="{{ asset('js//auth.js') }}"></script>

</body>
</html>