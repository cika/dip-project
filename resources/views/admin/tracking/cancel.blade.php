@extends('layouts.admin')
@section('content-header')
<h1>
   Cancel Tracking
</h1>
<ol class="breadcrumb">
   <li><a href="/admin"><i class="fa fa-dashboard"></i>Home</a></li>
   <li class="active">Cancel Tracking</li>
</ol>
@endsection
@section('content')
    <div class="box box-success">
        <div class="box-body">
            <div class="table-responsive">
                <table id="tracking_table" class="table table-striped table-bordered table-hover nowrap dataTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Email</th>
                            <th>Kode Booking</th>
                            <th>Last Updated</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
jQuery(document).ready(function($) {
        function dateCustom(date){
            var day   = date.substring(8,10);
            var mount = date.substring(5,7);
            var year  = date.substring(0,4);

            var mountChar = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

            return day + ' ' + mountChar[mount-1] + ' ' + year; 
        }

        var table = $('#tracking_table').DataTable({
            "bFilter": true,
            "processing": true,
            "serverSide": true,
            "lengthChange": true,
            "ajax": {
                "url": "{{ route('tracking.cancel') }}",
                "type": "POST",
                "data" : {}
            },
            "language": {
                "emptyTable": "No data available",
            },
            "columns": [
                {
                   data: null,
                   render: function (data, type, row, meta) {
                       return meta.row + meta.settings._iDisplayStart + 1;
                   },
                   "width": "20px",
                   "orderable": false,
                },
                {
                    "data": "email",
                    "orderable": true,
                },
                {
	                "data": "code",
	                "orderable": false,
	            },
                {
                    render : function(data, type, row){
                            return dateCustom(row.updated_at);
                    },
                    "orderable": true,
                },
                {
                    render :function(data, type, row){
                        if(row.status == 'Cancel')
                            return'<span class="badge bg-black">'+ row.status +'</span>';
                    },
	            },
            ],
            "order": [ 1, 'asc' ],
            "fnCreatedRow" : function(nRow, aData, iDataIndex) {
                $(nRow).attr('data', JSON.stringify(aData));
            }
        });

});
</script>
@endsection