@extends('layouts.admin')
@section('content-header')
<h1>
   Tracking
</h1>
<ol class="breadcrumb">
   <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
   <li class="active">Tracking</li>
</ol>
@endsection
@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <button id="btnAdd" class="pull-right btn btn-primary"><i class="fa fa-plus"></i> Add</button>
        </div>
       
        <div class="box-body">
            <div class="table-responsive">
                <table id="tracking_table" class="table table-striped table-bordered table-hover nowrap dataTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Email</th>
                            <th>Kode Booking</th>
                            <th>Last Updated</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <!-- add and edit -->
    <div class="modal fade" tabindex="-1" role="dialog" id="TraceModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="#" method="post" id="formtrace">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title"></h4>
                    </div>

                    <div class="modal-body">
                        <div class="form-horizontal">
                            <input type="hidden" id="id" name="id">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">
                                    email
                                </label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Input your Email" required>

                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">
                                    Note
                                </label>
                                <div class="col-sm-10">
                                     <textarea id="note" name="note" class="form-control" required></textarea>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"> 
                            Back
                        </button>
                        <button type="submit" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-spin'></i>">
                            Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- delete -->
    <div class="modal fade" tabindex="-1" role="dialog" id="deleteTrackingModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('tracking.destroy', ['tracking' => '#']) }}" method="post" id="formDeleteTracking">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Cancel Tracking</h4>
                    </div>

                    <div class="modal-body">
                        <p id="del-success">Are you sure you want to Cancel Tracking ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-spin'></i>">Yes
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- change -->
    <div class="modal fade" tabindex="-1" role="dialog" id="ChangeTrackingModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('tracking.change')}}" method="post" id="formChangeTracking">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Change Tracking</h4>
                    </div>

                    <div class="modal-body">
                        <input type="hidden" id="id" name="id">
                        <p id="del-success">Are you sure Change Next status ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-spin'></i>">Yes
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
jQuery(document).ready(function($) {

        @if (session('success'))
            toastr.success("{{ session('success') }}");
        @endif

        @if (session('error'))
            toastr.error("{{ session('error') }}");
        @endif

        function dateCustom(date){
                var day   = date.substring(8,10);
                var mount = date.substring(5,7);
                var year  = date.substring(0,4);

                var mountChar = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

                return day + ' ' + mountChar[mount-1] + ' ' + year; 
        }

        var table = $('#tracking_table').DataTable({
            "bFilter": true,
            "processing": true,
            "serverSide": true,
            "lengthChange": true,
            "ajax": {
                "url": "{{ route('tracking.index') }}",
                "type": "POST",
                "data" : {}
            },
            "language": {
                "emptyTable": "No data available",
            },
            "columns": [
                {
                   data: null,
                   render: function (data, type, row, meta) {
                       return meta.row + meta.settings._iDisplayStart + 1;
                   },
                   "width": "20px",
                   "orderable": false,
                },
                {
                    "data": "email",
                    "orderable": true,
                },
                {
	                "data": "code",
	                "orderable": false,
	            },
                {
                    render : function(data, type, row){
                        return dateCustom(row.updated_at);
                    },
                    "orderable": false,
                },
                {
                    render :function(data, type, row){
                        if(row.status == 'Received')
                            return'<span class="badge bg-red">'+ row.status +'</span>';
                        else if(row.status == 'Arrived')
                            return '<span class="badge bg-green">'+ row.status +'</span>';
                        else if(row.status == 'Process')
                            return '<span class="badge bg-orange">'+ row.status +'</span>';
                        else if(row.status == 'Editing')
                            return '<span class="badge bg-blue">'+ row.status +'</span>';
                        else (row.status == 'Done')
                            return '<span class="badge bg-purple">'+ row.status +'</span>';
                    },
	            },
                {
                    render : function(data, type, row){
                        if(row.status != 'Done'){              
                            return  '<a href="#" data-toggle="tooltip" title="Change" class="change-btn badge bg-blue"><i class="fa fa-exchange"></i></a> &nbsp;' +
                                    '<a href="#" data-toggle="tooltip" title="Edit" class="edit-btn badge bg-orange"><i class="fa fa-pencil"></i></a> &nbsp;' +
                                    '<a href="#" data-toggle="tooltip" title="Delete" class="delete-btn badge bg-red"><i class="fa fa-trash"></i></a>'
                                    ;
                            }
                            return '<a href="#" data-toggle="tooltip" title="Edit" class="edit-btn badge bg-orange"><i class="fa fa-pencil"></i></a> &nbsp;' +
                                    '<a href="#" data-toggle="tooltip" title="Delete" class="delete-btn badge bg-red"><i class="fa fa-trash"></i></a>'
                                    ;
                        },
                    "width": "10%",
                    "orderable": false,
                }
            ],
            "order": [ 1, 'asc' ],
            "fnCreatedRow" : function(nRow, aData, iDataIndex) {
                $(nRow).attr('data', JSON.stringify(aData));
            }
        });

        var url;

        $('#btnAdd').click(function () {
            $('#formtrace')[0].reset();
            $('#formtrace button[type=submit]').button('reset');
            $('#formtrace .modal-title').text("Add Slide");
            $('#formtrace div.form-group').removeClass('has-error');
            $('#formtrace .help-block').empty();

            $('#formtrace input[name="_method"]').remove();
            url = '{{ route("tracking.store") }}';

            var data = $('#formtrace').serializeArray();
            $.each(data, function(key, value){
                $("#formtrace input[name='" + data[key].name + "']").parent().find('.help-block').hide();
            });

            $('#TraceModal').modal('show');
        });

        $(document).on('submit','#formtrace',function(event) {
            event.preventDefault();
            var data = $('#formtrace').serialize();
            $('#formtrace button[type=submit]').button('loading');
            $.ajax({
                url: url,
                type: 'POST',
                data : data,
                cache : false,
                success: function(data){
                   if (data.success){
                      table.draw();
                      toastr.success(data.message);
                      $('#TraceModal').modal('hide');
                      $('#formtrace button[type=submit]').button('reset');
                   }
                },
                error: function(response){
                  toastr.error('Data tidak berhasil di simpan');
                  $('#formtrace button[type=submit]').button('reset');
                }
            })
        });



        //Edit
        $('#tracking_table').on('click', '.edit-btn', function(e){
            $('#formtrace .modal-title').text("Edit Tracking");
            $('#formtrace button[type=submit]').button('reset');
            $('#formtrace div.form-group').removeClass('has-error');
            $('#formtrace .help-block').empty();
            $('#formtrace')[0].reset();

            var aData = JSON.parse($(this).parent().parent().attr('data'));

            $('#formtrace .modal-body .form-horizontal').append('<input type="hidden" name="_method" value="PUT">');
            url = '{{ route("tracking.index") }}' + '/update/' + aData.id;

            var data = $('#formtrace').serializeArray();
            $.each(data, function(key, value){
                $("#formtrace input[name='" + data[key].name + "']").parent().find('.help-block').hide();
            });

            $('#id').val(aData.id);
            $('#email').val(aData.email).attr('disabled','disabled');
            $('#note').val(aData.note);
            $('#status').val(aData.status);
            $('#TraceModal').modal('show'); 
        });

        //Change
        $('#tracking_table').on('click', '.change-btn' , function(e){
            var aData = JSON.parse($(this).parent().parent().attr('data'));
            $('#formChangeTracking #id').val(aData.id);
            console.log(aData.id);
             $('#ChangeTrackingModal').modal('show');
        });

        $('#formChangeTracking').submit(function (event) {
            event.preventDefault();               
            $('#ChangeTrackingModal button[type=submit]').button('loading');
            var _data = $("#formChangeTracking").serialize();

            $.ajax({
                url: $('#formChangeTracking').attr('action'),
                type: 'POST',
                data: _data,
                dataType: 'json',
                cache: false,

                success: function (response) {
                    if (response.success) {
                        table.draw();
                            
                        toastr.success(response.message);
                        $('#ChangeTrackingModal').modal('toggle');
                    }
                    else{
                        toastr.error(response.message);
                    }
                    $('#ChangeTrackingModal button[type=submit]').button('reset');
                    $('#formChangeTracking')[0].reset();
                },
                error: function(response){
                    if (response.status === 400 || response.status === 422) {
                        // Bad Client Request
                        toastr.error(response.responseJSON.message);
                    } else {
                        toastr.error("Whoops, looks like something went wrong.");
                    }

                    $('#formChangeTracking button[type=submit]').button('reset');
                }
            });
        });

                // Delete
        $('#tracking_table').on('click', '.delete-btn' , function(e){
            var aData = JSON.parse($(this).parent().parent().attr('data'));
            url =  $('#formDeleteTracking').attr('action').replace('#', aData.id);
            $('#deleteTrackingModal').modal('show');
        });

        $('#formDeleteTracking').submit(function (event) {
            event.preventDefault();               
            $('#deleteTrackingModal button[type=submit]').button('loading');
            var _data = $("#formDeleteTracking").serialize();

            $.ajax({
                url: url,
                type: 'POST',
                data: _data,
                dataType: 'json',
                cache: false,

                success: function (response) {
                    if (response.success) {
                        table.draw();
                            
                        toastr.success(response.message);
                        $('#deleteTrackingModal').modal('toggle');
                    }
                    else{
                        toastr.error(response.message);
                    }
                    $('#deleteTrackingModal button[type=submit]').button('reset');
                    $('#formDeleteTracking')[0].reset();
                },
                error: function(response){
                    if (response.status === 400 || response.status === 422) {
                        // Bad Client Request
                        toastr.error(response.responseJSON.message);
                    } else {
                        toastr.error("Whoops, looks like something went wrong.");
                    }

                    $('#formDeleteTracking button[type=submit]').button('reset');
                }
            });
        });

});
</script>
@endsection