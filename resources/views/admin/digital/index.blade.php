@extends('layouts.admin')
@section('content-header')
<h1>
   Digital Imaging
</h1>
<ol class="breadcrumb">
   <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
   <li class="active">Digital Imaging</li>
</ol>
@endsection
@section('content')
  <div class="row">
      <form action="#" method="post"  id="FormDigital">
        
           <div class="box box-info">
              <div class="box-body pad">
                 <textarea id="digital" name="digital" class="form-control">{{$digital->content}}</textarea>
              </div>
           </div>
        
        <div class="box-tools pull-right">
            <button id="BtnSave" type="submit" class="btn btn-primary btn-approve"><i class="fa fa-save"></i> Save </button>
        </div>
      </form>
  </div>
@endsection
@section('js')
<script src="//cdn.ckeditor.com/4.11.1/full/ckeditor.js"></script>
<script type="text/javascript">   
    $(function () {
        CKEDITOR.replace('digital');
    });
  
</script>
<script type="text/javascript">
  jQuery(document).ready(function($) {
      $(document).on('submit','#FormDigital',function(event) {
        event.preventDefault();
        var data = $('#FormDigital').serialize();
        console.log(data)
        $.ajax({
            url: '/admin/digital',
            type: 'POST',
            data : data,
            cache : false,
            success: function(data){
               if (data.success){
                  toastr.success(data.message);
                  setTimeout(function () { 
                    location.reload();
                  }, 1000)
               }
            },
            error: function(response){
              toastr.error('Data tidak berhasil di simpan')
            }
        })
      });
  });
  </script>
  @endsection