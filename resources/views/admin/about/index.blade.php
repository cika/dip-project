@extends('layouts.admin')

@section('content-header')
<h1>
   About Us
</h1>
<ol class="breadcrumb">
   <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
   <li class="active">About Us</li>
</ol>
@endsection
@section('content')
  <div class="row">
      <form action="#" method="post" id="FormAbout">
           <div class="box box-info">
              <div class="box-body pad">
                @foreach($about as $item)
                <h4 style="text-transform: capitalize;">{{$item->name}}</h4>
                <textarea id="{{$item->name}}" name="{{$item->name}}" class="form-control">{{$item->content}}</textarea>
                @endforeach
              </div>
           </div>
        <div class="box-tools pull-right">
            <button id="BtnSave" type="submit" class="btn btn-primary btn-approve"><i class="fa fa-save"></i> Save </button>
        </div>
      </form>
  </div>
@endsection
@section('js')
<script src="//cdn.ckeditor.com/4.11.1/full/ckeditor.js"></script>
@include('ckfinder::setup')
<script type="text/javascript">   
    
    // CKFinder.setupCKEditor( editor );
    $(function () {
        CKEDITOR.replace('line1',{
            // Use named CKFinder browser route
              filebrowserBrowseUrl: '{{ route('ckfinder_browser') }}',
              // // Use named CKFinder connector route
              filebrowserUploadUrl: '{{ route('ckfinder_connector') }}?command=QuickUpload&type=Images'
        });
        CKEDITOR.replace('line2',{
            // Use named CKFinder browser route
              filebrowserBrowseUrl: '{{ route('ckfinder_browser') }}',
              // // Use named CKFinder connector route
              filebrowserUploadUrl: '{{ route('ckfinder_connector') }}?command=QuickUpload&type=Images'
        });
        CKEDITOR.replace('line3',{
            // Use named CKFinder browser route
              filebrowserBrowseUrl: '{{ route('ckfinder_browser') }}',
              // Use named CKFinder connector route
              filebrowserUploadUrl: '{{ route('ckfinder_connector') }}?command=QuickUpload&type=Images'
        });
    });
  
</script>
<script type="text/javascript">
  jQuery(document).ready(function($) {
      
      $('#FormAbout').submit(function (event) {
        event.preventDefault();
        for (instance in CKEDITOR.instances) {
          CKEDITOR.instances[instance].updateElement();
        }
        
        var data = $(this).serialize();
        console.log(data);
        $.ajax({
            url: '/admin/about',
            type: 'POST',
            data : data,
            success: function(data){
               if (data.success){
                  toastr.success(data.message);
                  setTimeout(function () { 
                    location.reload();
                  }, 1000)
               }
            },
            error: function(response){
              toastr.error('Data tidak berhasil di simpan')
            }
        })
      });
  });
  </script>
  @endsection