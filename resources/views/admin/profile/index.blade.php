@extends('layouts.admin')

@section('content_header')
	<h1>
		Profile User
	</h1>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-3">
			<div class="box box-success">
				<div class="box-body box-profile">
					<img class="profile-user-img img-responsive img-circle" src="{{asset('images/admin.png')}}" alt="User profile picture">

					<h3 class="profile-username text-center">{{ $user->name }}</h3>

					<ul class="list-group list-group-unbordered">
						<li class="list-group-item">
							<b>Email</b> <p class="pull-right">{{ $user->email }}</p>
						</li>
					</ul>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>

		<div class="col-md-9">
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#settings" data-toggle="tab">Pengaturan</a></li>
					<li><a href="#password" data-toggle="tab">Ubah Kata Sandi</a></li>
				</ul>
				<div class="tab-content">
					<!-- setting -->
					<div class="active tab-pane" id="settings">
						<form class="form-horizontal" method="post" action="" id="formSetting">
							<input type="hidden" id="id_setting" name="id" value="{{ $user->id }}">
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" id="name" name="name" placeholder="Nama">
									
									<span id="error_name" class="help-block"></span>
								</div>
							</div>
							
							<div class="form-group">
			                    <div class="col-sm-offset-2 col-sm-10">
			                      	<button type="submit" class="btn btn-danger">Simpan</button>
			                    </div>
		                  	</div>
						</form>

					</div>
					<!-- end setting -->

					<!-- password -->
					<div class="tab-pane" id="password">
						<form class="form-horizontal" method="post" action="" id="formPassword">
							<input type="hidden" id="id_password" name="id" value="{{ $user->id }}">
							<div class="form-group">
								<label class="col-sm-3 control-label">Kata Sandi Lama</label>
								<div class="col-sm-9">
									<input type="password" class="form-control" id="current_password" name="current_password" placeholder="Kata Sandi Lama">

									<span id="error_current_password" class="help-block"></span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Kata Sandi Baru</label>

								<div class="col-sm-9">
									<input type="password" class="form-control" id="new_password" name="new_password" placeholder="Kata Sandi Baru">

									<span id="error_new_password" class="help-block"></span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Ulangi Kata Sandi</label>

								<div class="col-sm-9">
									<input type="password" class="form-control" id="new_password_confirm" name="new_password_confirm" placeholder="Ulangi Kata Sandi">

									<span id="error_new_password_confirm" class="help-block"></span>
								</div>
							</div>
							
							<div class="form-group">
			                    <div class="col-sm-offset-3 col-sm-9">
			                      	<button type="submit" class="btn btn-danger">Simpan</button>
			                    </div>
		                  	</div>
						</form>

					</div>
					<!-- end password -->
				</div>
			<!-- /.tab-content -->
			</div>
			<!-- /.nav-tabs-custom -->
		</div>
		<!-- /.col -->
	</div>
@endsection

@section('js')
   	<script>
        jQuery(document).ready(function($) {
    		$('#formSetting').submit(function (event) {
                event.preventDefault();
			 	var _data = $("#formSetting").serialize();
			 	$.ajax({
                    url: '/admin/profile/setting',
                    type: 'POST',
                    data: _data,
                    cache: false,

                    success: function (data) {
                        if (data.success) {
        					$('#formSetting button[type=submit]').button('reset');
                            $('#formSetting')[0].reset();
                            toastr.success('Data Telah diSimpan');
                            setTimeout(function () { 
                                location.reload();
                            }, 1000);
                        }
                    },

 					error: function(data){
 						var error = data.responseJSON;
 						var data = $('#formSetting').serializeArray();
		            	$.each(data, function(key, value){
                            if( error.errors[data[key].name] != undefined ){
    		            		$('#error_' + data[key].name).text(error.errors[data[key].name]);
                                $('#error_' + data[key].name).show();
                                $('#' + data[key].name).parent().parent().addClass('has-error');
                                $('#formSetting button[type=submit]').button('reset');
                            }
		            	});
 					}
                });
    		});


    		$('#formPassword').submit(function (event) {
    			event.preventDefault();
    			$('#formPassword div.form-group').removeClass('has-error');
		        $('#formPassword .help-block').empty();
		        $('#formPassword div.form-group').removeClass('has-error');
		        $('#formPassword .help-block').empty();
			 	var _data = $("#formPassword").serialize();
			 	$.ajax({
                    url: '/admin/profile/password',
                    type: 'POST',
                    data: _data,
                    cache: false,

                    success: function (data) {
                        if (data.success) {
        					$('#formPassword button[type=submit]').button('reset');
                            $('#formPassword')[0].reset();
                            toastr.success('password Telah Di ganti Telah diSimpan');
                            setTimeout(function () { 
                                location.reload();
                            }, 1000);
                        }
                        else{
                        	$('#error_current_password').text(data.message);
                            $('#error_current_password').show();
                            $('#current_password').parent().parent().addClass('has-error');
                        }
                    },

 					error: function(data){
 						var error = data.responseJSON;
 						var data = $('#formPassword').serializeArray();
		            	$.each(data, function(key, value){
                            if( error.errors[data[key].name] != undefined){
    		            		$('#error_' + data[key].name).text(error.errors[data[key].name]);
                                $('#error_' + data[key].name).show();
                                $('#' + data[key].name).parent().parent().addClass('has-error');
                                $('#formPassword button[type=submit]').button('reset');
                            }
		            	});
 					}
                });
    		});
    	});
   	</script>
@endsection