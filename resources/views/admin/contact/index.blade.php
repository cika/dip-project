@extends('layouts.admin')

@section('content-header')
	<h1>
		Change Profile Web
	</h1>
	<ol class="breadcrumb">
    <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><a href="#">Change Profile Web</a></li>
  </ol>
@endsection

@section('content')
	<div class="box box-success">
		<form action="#" method="post" id="formContact">
			<div class="box-body">
				<div class="form-group">
					<label class="col-sm-2 control-label"><i class="fa fa-map-marker"></i> Address</label>
					<div class="col-sm-12">
						<input type="text" class="form-control" id="address" name="address" placeholder="Input your address" value="@if( $data != null ) {{ $data->address }} @endif">

						<span class="help-block"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label"><i class="fa fa-phone"></i> Number Phone</label>
					<div class="col-sm-12">
						<input type="text" class="form-control" id="phone" name="phone" placeholder="Input your number phone" value="@if( $data != '' ) {{ $data->phone }} @endif">

						<span class="help-block"></span>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label"><i class="fa fa-envelope"></i> Email</label>
					<div class="col-sm-12">
						<input type="email" class="form-control" id="email" name="email" placeholder="Input your email" value="@if( $data != '' ) {{ $data->email }} @endif">

						<span class="help-block"></span>
					</div>
				</div>

                 <div class="form-group">
                    <label class="col-sm-2 control-label"><i class="fa fa-globe"></i> Website </label>
                    <div class="col-sm-12">
                        <input type="url" class="form-control" id="web" name="web" placeholder="Input your Web" value="@if( $data != '' ) {{ $data->web }} @endif">

                        <span class="help-block"></span>
                    </div>
                </div>
			</div>


            <div class="box-footer">
                <div class="form-group">
                    <div class="pull-right">
                      	<div class="col-sm-1">
                      		<button type="submit" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-spin'></i>"> Update</button>
                      	</div>
                    </div>
              	</div>
            </div>
    	</form>
	</div>
@endsection

@section('js')
	<script type="text/javascript">
		jQuery(document).ready(function($){

			@if (session('success'))
                toastr.success("{{ session('success') }}");
            @endif

            @if (session('error'))
                toastr.error("{{ session('error') }}");
            @endif

			$('#formContact').submit(function(event){
				event.preventDefault();
				$('#formContact div.form-group').removeClass('has-error');
		        $('#formContact .help-block').empty();
			 	$('#formContact button[type=submit]').button('loading');

				var _data = $('#formContact').serialize();

				$.ajax({
                    url: '{{ route("contact.index") }}',
                    type: 'POST',
                    data: _data,
                    dataType: 'json',
                    cache: false,

                    success: function (response) {
                        $('#formContact button[type=submit]').button('reset');
                        $('#formContact')[0].reset();

                        toastr.success(response.message);

                        setTimeout(function () { 
                            location.reload();
                        }, 2000);
                    },

                    error: function (response) {
                    	if (response.status === 422) {
                            // form validation errors fired up
                            var error = response.responseJSON.errors;
                            var data = $('#formContact').serializeArray();
                            $.each(data, function(key, value){
                                if( error[data[key].name] != undefined ){
                                    var elem = $("#formContact input[name='" + data[key].name + "']").length ? $("#formContact input[name='" + data[key].name + "']") : $("#formContact select[name='" + data[key].name + "']");
                                    elem.parent().find('.help-block').text(error[data[key].name]);
                                    elem.parent().find('.help-block').show();
                                    elem.parent().parent().addClass('has-error');
                                }
                            });
                        }
                        else if (response.status === 400) {
                            // Bad Client Request
                            toastr.error(response.responseJSON.message);
                        }
                        else {
                            toastr.error("Whoops, looks like something went wrong.");
                        }

                        $('#formContact button[type=submit]').button('reset');
                    }
                });
			});
		});
	</script>
@endsection