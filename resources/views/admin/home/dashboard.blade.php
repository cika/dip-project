@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-lg-2 col-md-2  col-md-offset-1">
          <div class="small-box bg-red">
            <div class="inner">
                <h3>{{$received}}</h3>
                <p>Received</p>
            </div>
            <div class="icon">
              <i class="fa fa-caret-square-o-down"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-2 col-md-2">
          <div class="small-box bg-green">
            <div class="inner">
                <h3>{{$arrived}}</h3>
                <p>Arrived</p>
            </div>
            <div class="icon">
              <i class="fa fa-map-marker"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-2 col-md-2">
          <div class="small-box bg-yellow">
            <div class="inner">
                <h3>{{$process}}</h3>
                <p>Process</p>
            </div>
            <div class="icon">
              <i class="fa fa-recycle"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-2 col-md-2">
          <div class="small-box bg-blue">
            <div class="inner">
                <h3>{{$editing}}</h3>
                <p>Editing</p>
            </div>
            <div class="icon">
              <i class="fa fa-pencil-square-o"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-2 col-md-2">
          <div class="small-box bg-purple">
            <div class="inner">
                <h3>{{$done}}</h3>
                <p>Done</p>
            </div>
            <div class="icon">
              <i class="fa fa-check-circle-o"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
    </div>

@endsection
@section('js')
    <script>
        jQuery(document).ready(function($) {
            @if (session('success'))
              toastr.success("{{ session('success') }}");
            @endif

            @if (session('error'))
              toastr.error("{{ session('error') }}");
            @endif
        });
    </script>
@endsection
