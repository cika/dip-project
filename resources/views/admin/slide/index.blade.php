@extends('layouts.admin')
@section('content-header')
<h1>
   Slide
</h1>
<ol class="breadcrumb">
    <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><a href="#">slide</a></li>
</ol>
@endsection
@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <button id="btnAdd" class="pull-right btn btn-primary"><i class="fa fa-plus"></i> Add</button>
        </div>
       
        <div class="box-body">
            <div class="table-responsive">
                <table id="slide_table" class="table table-striped table-bordered table-hover nowrap dataTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Title</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <!-- add and edit -->
    <div class="modal fade" tabindex="-1" role="dialog" id="slideModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="#" method="post" id="formSlide" enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title"></h4>
                    </div>

                    <div class="modal-body">
                        <div class="form-horizontal">
                            <input type="hidden" id="id" name="id">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">
                                    Title
                                </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="title" name="title" placeholder="Input your title" required>

                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">
                                    Description
                                </label>
                                <div class="col-sm-10">
                                     <textarea id="description" name="description" class="form-control" required></textarea>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">
                                    Image
                                </label>
                                                            
                                <div class="fileinput fileinput-new col-sm-10" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                        <img id="img-upload">
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                    <div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileinput-new">Select Image</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" id="image_id" name="image_id" accept="image/x-png,image/jpeg">
                                        </span>
                                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">
                                            Remove
                                        </a>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"> 
                            Back
                        </button>
                        <button type="submit" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-spin'></i>">
                            Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- delete -->
    <div class="modal fade" tabindex="-1" role="dialog" id="deleteSlideModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('slide.destroy', ['slide' => '#']) }}" method="post" id="formDeleteSlide">
                    {{ method_field('DELETE') }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Delete Slide</h4>
                    </div>

                    <div class="modal-body">
                        <p id="del-success">Are you sure you want to remove Slide ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-spin'></i>">Yes
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- view -->
    <div class="modal fade" tabindex="-1" role="dialog" id="viewSlideModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"></h4>
                </div>

                <div class="modal-body">
                    <center>
                        <img id="img-modal" class="thumbnail" style="width: 400px; height: 300px;">
                    </center>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
@endsection

@section('js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function($) {

            @if (session('success'))
                toastr.success("{{ session('success') }}");
            @endif

            @if (session('error'))
                toastr.error("{{ session('error') }}");
            @endif

            var table = $('#slide_table').DataTable({
                "bFilter": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "ajax": {
                    "url": "{{ route('slide.index') }}",
                    "type": "POST",
                    "data" : {}
                },
                "language": {
                    "emptyTable": "No data available",
                },
                "columns": [
                    {
                       data: null,
                       render: function (data, type, row, meta) {
                           return meta.row + meta.settings._iDisplayStart + 1;
                       },
                       "width": "20px",
                       "orderable": false,
                    },
                    {
                        "data": "title",
                        "orderable": true,
                    },
                    {
                        render : function(data, type, row){
                            return  '<a href="#" data-toggle="tooltip" title="View" class="view-btn badge bg-blue"><i class="fa fa-eye"></i></a> &nbsp;' +
                                    '<a href="#" data-toggle="tooltip" title="Edit" class="edit-btn badge bg-orange"><i class="fa fa-pencil"></i></a> &nbsp;' +
                                    '<a href="#" data-toggle="tooltip" title="Delete" class="delete-btn badge bg-red"><i class="fa fa-trash"></i></a>'
                                    ;
                        },
                        "width": "10%",
                        "orderable": false,
                    }
                ],
                "order": [ 1, 'asc' ],
                "fnCreatedRow" : function(nRow, aData, iDataIndex) {
                    $(nRow).attr('data', JSON.stringify(aData));
                }
            });

            var url;

            // Add
            $('#btnAdd').click(function () {
                $('#formSlide')[0].reset();
                $('#formSlide button[type=submit]').button('reset');
                $('#formSlide .modal-title').text("Add Slide");
                $('#formSlide div.form-group').removeClass('has-error');
                $('#formSlide .help-block').empty();
                $('#formSlide #img-upload').attr('src', 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image');

                $('#formSlide input[name="_method"]').remove();
                url = '{{ route("slide.store") }}';

                var data = $('#formSlide').serializeArray();
                $.each(data, function(key, value){
                    $("#formSlide input[name='" + data[key].name + "']").parent().find('.help-block').hide();
                });

                $('#slideModal').modal('show');
            });

            // Edit
            $('#slide_table').on('click', '.edit-btn', function(e){
                $('#formSlide .modal-title').text("Edit Slide");
                $('#formSlide button[type=submit]').button('reset');
                $('#formSlide div.form-group').removeClass('has-error');
                $('#formSlide .help-block').empty();
                $('#formSlide')[0].reset();

                var aData = JSON.parse($(this).parent().parent().attr('data'));

                $('#formSlide .modal-body .form-horizontal').append('<input type="hidden" name="_method" value="PUT">');

                
                url = '{{ route("slide.index") }}' + '/update/' + aData.id;

                
                var data = $('#formSlide').serializeArray();

                $.each(data, function(key, value){
                    $("#formSlide input[name='" + data[key].name + "']").parent().find('.help-block').hide();
                });

                $('#id').val(aData.id);
                $('#title').val(aData.title);
                $('#description').val(aData.description);

                if( aData.image != null )
                    $('#formSlide #img-upload').attr('src', "{{ asset('storage') }}" + '/' + aData.image.path);
                else
                    $('#formSlide #img-upload').attr('src', 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image');

                $('#slideModal').modal('show');              
            });


            $('#formSlide').submit(function (event) {
                event.preventDefault();
                $('#formSlide button[type=submit]').button('loading');
                $('#formSlide div.form-group').removeClass('has-error');
                $('#formSlide .help-block').empty();

                var _data = $("#formSlide").serialize();
                var formData = new FormData($("#formSlide")[0]);

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: formData,
                    processData : false,
                    contentType : false,   
                    cache: false,

                    success: function (response) {
                        if (response.success) {
                            table.draw();
                            toastr.success(response.message);
                            $('#slideModal').modal('hide');
                        }
                        else{
                            toastr.error(response.message);                            
                        }
                        $('#formSlide button[type=submit]').button('reset');
                    },

                    error: function(response){
                        if (response.status === 422) {
                            // form validation errors fired up
                            var error = response.responseJSON.errors;
                            var data = $('#formSlide').serializeArray();
                            $.each(data, function(key, value){
                                if( error[data[key].name] != undefined ){
                                    var elem = $("#formSlide input[name='" + data[key].name + "']").length ? $("#formSlide input[name='" + data[key].name + "']") : $("#formSlide select[name='" + data[key].name + "']");
                                    elem.parent().find('.help-block').text(error[data[key].name]);
                                    elem.parent().find('.help-block').show();
                                    elem.parent().parent().addClass('has-error');
                                }
                            });
                            if(error['image_id'] != undefined){
                                $("#formSlide input[name='image_id']").parent().parent().parent().find('.help-block').text(error['image_id']);
                                $("#formSlide input[name='image_id']").parent().parent().parent().find('.help-block').show();
                                $("#formSlide input[name='image_id']").parent().parent().parent().parent().addClass('has-error');
                            }
                        }
                        else if (response.status === 400) {
                            // Bad Client Request
                            toastr.error(response.responseJSON.message);
                        }
                        else if (response.status === 413) {
                            toastr.error("File size too large.");
                        }
                        else {
                            toastr.error("Whoops, looks like something went wrong. Please try again later.");
                        }
                        $('#formSlide button[type=submit]').button('reset');
                    }
                });
            });

            // Delete
            $('#slide_table').on('click', '.delete-btn' , function(e){
                var aData = JSON.parse($(this).parent().parent().attr('data'));
                url =  $('#formDeleteSlide').attr('action').replace('#', aData.id);
                $('#deleteSlideModal').modal('show');
            });

            $('#formDeleteSlide').submit(function (event) {
                event.preventDefault();               
                $('#deleteSlideModal button[type=submit]').button('loading');
                var _data = $("#formDeleteSlide").serialize();

                $.ajax({
                    url: url,
                    type: 'DELETE',
                    data: _data,
                    dataType: 'json',
                    cache: false,

                    success: function (response) {
                        if (response.success) {
                            table.draw();
                                
                            toastr.success(response.message);
                            $('#deleteSlideModal').modal('toggle');
                        }
                        else{
                            toastr.error(response.message);
                        }
                        $('#deleteSlideModal button[type=submit]').button('reset');
                        $('#formDeleteSlide')[0].reset();
                    },
                    error: function(response){
                        if (response.status === 400 || response.status === 422) {
                            // Bad Client Request
                            toastr.error(response.responseJSON.message);
                        } else {
                            toastr.error("Whoops, looks like something went wrong.");
                        }

                        $('#formDeleteSlide button[type=submit]').button('reset');
                    }
                });
            });

            // View
            $('#slide_table').on('click', '.view-btn' , function(e){
                var aData = JSON.parse($(this).parent().parent().attr('data'));
                $('#viewSlideModal .modal-title').text(aData.title);

                if( aData.image != null )
                    $('#viewSlideModal #img-modal').attr('src', "{{ asset('storage') }}" + '/' + aData.image.path);
                else
                    $('#viewSlideModal #img-modal').attr('src', 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image');

                $('#viewSlideModal').modal();
            });
        });
    </script>
@endsection