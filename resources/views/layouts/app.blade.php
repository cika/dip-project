<!DOCTYPE HTML>
<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>DIP-Project | Content & Production Service Solution</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Content & Production Service Solution" />
    <meta name="keywords" content="dip-project, digital project studio, fashion shoot, photo product, jasa foto katalog, fashion gram, dip-indonesia, dip, videography, Website" />
    <meta name="author" content="dip-project" />
    <link rel="icon" type="image/png" href="{{asset('images/logo.png')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/icomoon.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ mix('/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/animate.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ mix('/css/main.css') }}">
    <script src="{{ asset('/js/modernizr-2.6.2.min.js') }}"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    {{-- Development --}}
    <script async src="https://www.googletagmanager.com/gtag/js?id={{env('GA_TRACK_ID')}}"></script>
    <script>
     window.dataLayer = window.dataLayer || [];
     function gtag(){dataLayer.push(arguments);}
     gtag('js', new Date());

     gtag('config', '{{env("GA_TRACK_ID")}}');
    </script>
    <!-- End Google Analytics -->      

    </head>
    <body>
    <div class="fh5co-loader"></div>
    
    <div id="page">
    <nav class="fh5co-nav" role="navigation">
        <div class="top-menu">
            <div class="container">
                <div class="row">
                    <div class="col-xs-2">
                        <div id="fh5co-logo"><a href="/"><img src="{{asset('images/logo.png')}}"></a></div>
                    </div>
                    <div class="col-xs-10 text-right menu-1">
                        <ul>
                            <li class="{{ setActive('/', 'active') }}"><a href="/">Home</a></li>
                            <li class="has-dropdown {{ setActive('packages', 'active') }} ">
                                <a href="#">Packages</a>
                                <ul class="dropdown">
                                    <li><a href="/packages">Photography</a></li>
                                    <li><a href="/packages/web">Web</a></li>
                                </ul>
                            </li>
                            <li class="{{ setActive('digital', 'active') }}"><a href="/digital">Digital Imaging</a></li>
                            <li class="{{ setActive('about', 'active') }}"><a href="/about">About</a></li>
                            <li class="{{ setActive('contact', 'active') }}"><a href="/contact">Contact</a></li>
                            <li class="btn-cta"><a href="/trace"><span>Tracking</span></a></li>
                        
                        </ul>
                    </div>
                </div>
                
            </div>
        </div>
    </nav>
    @yield('css')
        <div style="margin-top: 100px;">
    @yield('content')
        </div>
        <footer id="fh5co-footer" style="padding: 2em 0;" role="contentinfo">
        <div class="container">
            <div class="row copyright">
                <div class="col-md-12 text-center">
                    <p>
                        <small class="block">&copy; 2019 All Rights Reserved.</small> 
                        <small class="block">Designed by <a href="http://dip-project/" target="_blank">dip-project.com</a> 
                    </p>
                    <p>
                        <ul class="fh5co-social-icons">
                        {{--<li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li> --}}
                            <li><a href="https://www.instagram.com/dip.id/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </p>
                </div>
            </div>

        </div>
    </footer>
    </div>
    
    <a href="https://api.whatsapp.com/send?phone=081384160557&text=halo%21%20DIP-PROJECT" class="float" target="_blank">
    <i class="fa fa-whatsapp my-float"></i>
    </a>

    <!-- <div class="gototop js-top">
        <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
    </div> -->

    <script src="{{ mix('/js/jquery.min.js') }}"></script>
    <script src="{{ mix('/js/jquery.easing.min.js') }}"></script>
    <script src="{{ mix('/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.waypoints.min.js') }}"></script>
    <script src="{{ mix('/js/jquery.stellar.js') }}"></script>
    <script src="{{ mix('/js/owl.carousel.min.js') }}"></script>
    <script src="{{ mix('/js/jquery.flexslider-min.js') }}"></script>
    <script src="{{ mix('/js/jquery.countTo.js') }}"></script>
    <script src="{{ mix('/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ mix('js/toastr.min.js') }}"></script>
    <script src="{{ asset('/js/main.js') }}"></script>

    <script>
         $.ajaxSetup({
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             }
         });

       toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
      }
    </script>
    
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-7572239184669892",
          enable_page_level_ads: true
        });
    </script>
    @yield('js')
    </body>
</html>
