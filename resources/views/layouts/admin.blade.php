<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>{{ config('app.name') }} | AdminPanel</title>
      {{-- <link rel="icon" type="image/png" href="{{asset('images/client/logo.png')}}"> --}}
      <!-- Styles -->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
      <link rel="stylesheet" type="text/css" href="{{ mix('/css/bootstrap.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ mix('/css/app.css') }}">
       @yield('css')
   </head>
   <body class="hold-transition sidebar-mini skin-yellow-light">
      <div class="wrapper">
         <!-- Main Header -->
         <header class="main-header">
            <!-- Logo -->
            <a href="/admin" class="logo">
               <!-- mini logo for sidebar mini 50x50 pixels -->
               <!-- logo for regular state and mobile devices -->
               <span class="logo-mini"><b>DIP</b> PRO</span>
               <span class="logo-lg"><b>{{ config('app.name') }}</b> admin</span>
            </a>
            <nav class="navbar navbar-static-top" role="navigation">
               <a href="/admin" class="sidebar-toggle" data-toggle="push-menu" role="button">
               <span class="sr-only">Toggle navigation</span>
               </a>
               <div class="navbar-custom-menu">
                  <ul class="nav navbar-nav">

                     <!-- User Account Menu -->
                     <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                           <!-- The user image in the navbar-->
                           <img src="{{asset('images/admin.png')}}" class="user-image" alt="User Image">
                           <!-- hidden-xs hides the username on small devices so only the image appears. -->
                           <span class="hidden-xs"> {{ Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                           <!-- The user image in the menu -->
                           <li class="user-header">
                              <img src="{{asset('images/admin.png')}}" class="img-circle" alt="User Image">
                              <p>
                                 {{ Auth::user()->name }}
                                 <small>Member since {{ date_format(Auth::user()->created_at, 'M. Y') }}</small>
                              </p>
                           </li>
                           <li class="user-footer">
                              <div class="pull-left">
                                 <a href="/admin/profile/{{ Auth::user()->id }}" class="btn btn-default btn-flat">Profile</a>
                              </div>
                              <div class="pull-right">
                                 <a href="{{ route('admin.logout') }}" class="btn btn-default btn-flat"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                 Sign out
                                 </a>
                                 <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                 </form>
                              </div>
                           </li>
                        </ul>
                     </li>
                  </ul>
               </div>
            </nav>
         </header>
         <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
               <div class="user-panel">
                <div class="pull-left image">
                 <img src="{{asset('images/admin.png')}}" class="img-circle" alt="User Image">
              </div>
              <div class="pull-left info">
                 <p>{{ Auth::user()->name }}</p>
                 <!-- Status -->
                 <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
              </div>
           </div>
            {!! $MyNavBar->asUl( ['class' => 'sidebar-menu', 'data-widget' => 'tree'],['class' => 'treeview-menu']) !!}
               <!-- /.sidebar-menu -->
            </section>
            <!-- /.sidebar -->
         </aside>
         <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
               @yield('content-header')
            </section>
            <!-- Main content -->
            <section class="content container-fluid">
               @yield('content')
            </section>
            <!-- /.content -->
         </div>
         <!-- /.content-wrapper -->
         <!-- Main Footer -->
         <footer class="main-footer">
            <!-- To the right -->
            <div class="pull-right hidden-xs">
               Version 1.0.0
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2018 <a href="#">{{ config('app.name') }}</a>.</strong> All rights reserved.
         </footer>
         <!-- Control Sidebar -->
         <!-- /.control-sidebar -->
         <!-- Add the sidebar's background. This div must be placed
            immediately after the control sidebar -->
         <div class="control-sidebar-bg"></div>
      </div>
      <!-- Scripts -->      
      <script src="{{ mix('js/jquery.min.js') }}"></script>
      <script src="{{ mix('js/bootstrap.min.js') }}"></script>
      <script src="{{ mix('js/adminlte.min.js') }}"></script>
      <script src="{{ mix('js/jquery.dataTables.js') }}"></script>
      <script src="{{ mix('js/dataTables.bootstrap.js') }}"></script>
      <script src="{{ mix('js/jquery.tagsinput.min.js') }}"></script>
      <script src="{{ mix('js/toastr.min.js') }}"></script>
      {{-- <script src="//cdn.ckeditor.com/4.11.1/full/ckeditor.js"></script> --}}
      <script>
         $.ajaxSetup({
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             }
         });
         
         Number.prototype.format = function(n, x) {
            var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
            return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
        };

          toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
          }
      </script>
      @yield('js')
   </body>
</html>