<html>
<head>
    <meta charset="utf-8">
    <title>dip-project</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <style type="text/css">
        .button {
            display: block;
            width: auto;
            padding: 20px 40px;
            min-width: 100px;
            max-width: 70%;
            background: #a9cbbb;
            color: #fff !important;
            text-align: center;
            border-radius: 5px;
            color: white;
            font-weight: bold;
            text-decoration: none;
            white-space: nowrap;
        }
        *{  
            font-family: 'Montserrat', sans-serif;
            color: #3d4466 !important;
            font-weight: bold !important;
        }
        body{
            margin: 0 auto;
            padding: 0px;
            height: auto;
        }
        h1,h2,h3,h4,h5,p,img{
            text-align: center;
        }
        p, span{
            font-size: 8.5pt;
        }
        h3{
            font-size: 1.45vw;
        }
        div, img{
            margin: 0 auto;
            padding: 0px;

        }
        table{
            background-color: #f0efde;
        }
        .main{
            width: 100%;
            font-size: 8.5pt !important;
            height: auto;
            color: #3d4466 !important;
            font-weight: bold;
            background-color: #f0efde;
        }
        .header{
            height: auto;
            background-color: #f0efde;
        }
        .body{
            height: auto;
            margin-top: 35px;
            background-color: #f0efde;
        }
        img{
            /* width: 10%; */
            display: block;
            text-align: center;
            height: auto;
        }
        img.logo{
            padding-top: 20px;
        }
        i.icon{
            display: inline;
            width: 2%;
            height: auto;
        }
        .footer{
            height: auto;
            width: 100%;
            padding-bottom: 20px;
            background-color: #f0efde;
        }
        #info{
            color: #8cc9a4 !important;
            font-weight: bolder;
            font-size: 11.5pt !important;
            /* font-size: 5vw; */
            /* line-height: 0px; */
            text-transform: uppercase;
            /* text-shadow: 6px 8px 1px #a9cbbb; */
        }
        .pull-left{
            float: left;
        }
        .pull-right{
            float: right;
        }
    </style>
</head>
<body>
    <div class="container main text-center align-middle">
        <div class="row">
            <table width="600" cellpadding="10" cellspacing="0" border="0" align="center" bgcolor="#FFFFFF" class="boxshadow">
                <tr align="center">
                    <td colspan="30">
                        <a href="www.dip-project.com">
                            <img style="width: 100px" class="logo" src="images/logo.png">
                        </a>
                    </td>
                </tr>
            </table>
            
            <table width="600" cellpadding="10" cellspacing="0" border="0" align="center" bgcolor="#ffffff" class="boxshadow">
                <tr height="0" align="left">
                    <td style="padding: 0 30px;">
                        <h5 style=" font-family:Tahoma, Geneva, sans-serif; line-height: 16px; font-size: 18px; color:#000; font-weight: 500;">
                            Your Project Has Finished</b> <br>
                            Booking Number :{{$tracking->code}}
                            <br>
                        </h5>
                    </td>
                </tr>
            </table>
        </div>

        <br><br><br>
        <div class="row footer">
            <table width="600" cellpadding="10" cellspacing="0" border="0" align="center" bgcolor="#FFFFFF" class="boxshadow">
                <tr align="center">
                    <td colspan="30">
                        <span class="" style="font-size: 9px !important; margin-top: -3px; margin-bottom: 2px">CONTACT</span> &nbsp;<img src="images/wa.png" style="display:inline-block; width: 12px; padding-top: 10px" alt=""><span class="" style="font-size: 9px !important; margin-top: -3px">&nbsp; +62 813 84160557</span><br>
                        <span class="" style="font-size: 9px !important">
                            <a href="www.dip-project.com" style="color: #3d4466 !important; text-decoration: none !important">www.dip-project.com</a>
                        </span>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>