@extends('layouts.app')
@section('content')
<div id="fh5co-contact">
   <div class="container">
      <h3>Terms and conditions</h3>
      <div class="p-termcondition">
         <p class="p-term">Welcome to www.dip-project.com</p>
         <p class="p-term">The terms & conditions set out below govern the use of services offered by DIP-PROJECT is related to the use of the site www.dip-project.com Users are advised to read carefully because it can have an impact on the rights and obligations of Users under the law. </p>
         <p class="p-term">By registering and / or using the site www.dip-project.com the user is deemed to have read, understood, understood and approved all contents in the Terms & Conditions. These terms & conditions constitute a form of agreement as outlined in a legal agreement between the User and PT.Topedia. If the user does not approve one, part, or all of the contents of the Terms & Conditions, the user is not permitted to use the service at www.dip-project.com</p>
      </div>
      <ul>
         <li class="conditions">A. Definition</li>
         <li class="conditions">B. Delivery</li>
         <li class="conditions">C. Updates</li>
      </ul>
      <br>
      <h5>A. Definition</h5>
      <ol>
         <li class="term">DIP-PROJECT is a limited liability company that runs the business activities of the web portal www.Putra Karo Mandiri.com, the store search site and goods sold by registered sellers. Next is called Putra Karo Mandiri.
            The Putra Karo Mandiri site is www.dip-project.com 
         </li>
         <li class="term">Terms & conditions is an agreement between User and Putra Karo Mandiri which contains a set of li governing the rights, obligations, responsibilities of users and Putra Karo Mandiri, and procedures for using the Putra Karo Mandiri service system. </li>
         <li class="term">Users are parties who use Putra Karo Mandiri services, including but not limited to buyers, sellers or other parties who are just visiting the Putra Karo Mandiri Site. </li>
         <li class="term">The Buyer is a registered User who requests for Goods sold by the Seller on the Putra Karo Mandiri Site. Seller is a registered User who takes action to open a store and / or bid for an item to Putra Karo Mandiri Site Users.</li>
         <li class="term"> Goods are objects that are tangible / have physical goods that can delivered / li the shipping criteria by the freight forwarding company. Putra Karo Mandiri Website Feed is a feature on the Putra Karo Mandiri Website that displays KOL promotions for certain Items or Sellers.</li>
         <li class="term">Key Opinion Leaders or KOL are those who promote certain Items or Sellers through Putra Karo Mandiri Website Feed. Putra Karo Mandiri Official Account is a joint account agreed upon by Putra Karo Mandiri and users to process buying and selling transactions on the Putra Karo Mandiri Site. </li>
         <li class="term">The official Putra Karo Mandiri account can be found on the https://www.dip-project.com/li>
         <li class="term">Internal Dropshipper is a transaction that takes place entirely within the scope of Putra Karo Mandiri done by the Seller by buying other Seller Items and reselling them through the Putra Karo Mandiri Site.</li>
      </ol>
      <h5>B. Delivery</h5>
      <ol>
         <li class="term">Delivery of goods in the Putra Karo Mandiri system is obliged to use the services of an expedition company that has obtained verification of a Putra Karo Mandiri partner chosen by the Buyer.</li>
         <li class="term">The seller must fulfill the conditions set by the freight forwarding service and be responsible for each item shipped.</li>
         <li class="term"> Each provision regarding the process of shipping goods is fully authorized by the shipping service provider.</li>
      </ol>
      <h5>C. Updates</h5>
      <div class="p-termcondition">
         <p class="p-term">Terms & conditions may be changed and / or updated from time to time without prior notice. Putra Karo Mandiri recommends that you read carefully and check this Terms & conditions page from time to time to find out about any changes. By continuing to access and use Putra Karo Mandiri services, users are deemed to agree to changes in the Terms & conditions.</p>
      </div>
   </div>
</div>
@endsection