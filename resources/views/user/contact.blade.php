@extends('layouts.app')
@section('content')
	<div id="fh5co-container">
		<div class="container">
			<div class="row">
				<div class="col-md-5 animate-box">
					
					<div class="fh5co-contact-info">
						<h3>Contact Information</h3>
						<ul>
							@foreach($contact as $item)
							<li class="address">{{$item->address}}</li>
							<li class="phone"><a href="tel://{{$item->phone}}">{{$item->phone}}</a></li>
							<li class="email"><a href="mailto:{{$item->email}}">{{$item->email}}</a></li>
							<li class="url"><a href="{{$item->web}}">{{$item->web}}</a></li>
							@endforeach
						</ul>
					</div>

				</div>
				<div class="col-md-6 animate-box">
					<h3>Send A Message</h3>
					<form action="#" id="FormSend">
						<div class="row form-group">
							<div class="col-md-12">
								<label for="fname">Name</label>
								<input type="text" id="name" name="name" class="form-control" placeholder="Your Name" required>
							</div>
						</div>

						<div class="row form-group">
							<div class="col-md-12">
								<label for="email">Email</label>
								<input type="text" id="email" name="email" class="form-control" placeholder="Your email address" required>
							</div>
						</div>

						<div class="row form-group">
							<div class="col-md-12">
								<label for="subject">Subject</label>
								<input type="text" id="subject" name="subject" class="form-control" placeholder="Your subject of this message" required>
							</div>
						</div>

						<div class="row form-group">
							<div class="col-md-12">
								<label for="message">Message</label>
								<textarea name="message" id="message" cols="30" rows="10" class="form-control" placeholder="Say something" required></textarea>
							</div>
						</div>
						<div class="form-group">
							<button type="submit"  class="btn btn-primary"> Send Message</button>
						</div>

					</form>		
				</div>
			</div>
			
		</div>
	</div>

<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d63460.85604963046!2d106.77942147412286!3d-6.223653496618703!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sid!4v1547117077916" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>

@endsection

@section('js')
<script>

	jQuery(document).ready(function($) {
        @if (session('success'))
            toastr.success("{{ session('success') }}");
        @endif

        @if (session('error'))
            toastr.error("{{ session('error') }}");
        @endif

		$('#FormSend').submit(function (event) {
	          event.preventDefault();
	          var form = $(this);
	          var data = new FormData($('#FormSend')[0]);
	          $('#FormSend button[type=submit]').button('loading');
	          $.ajax({
	              url: '/contactus',
	              type: 'POST',
	              data : data,
	              cache : false,
	              contentType : false,
	              processData : false,
	              success: function(data){
	                 if (data.success){
	                    $('#FormSend')[0].reset();
	                    $('#FormSend button[type=submit]').button('reset');
	                     toastr.success(data.message);
	                 }
	              },
	              error: function(response) {
	                if(response.status === 422){
	                }
	              }
	          })
        });
	});
</script>
@endsection