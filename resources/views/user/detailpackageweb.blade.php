@extends('layouts.app')
@section('css')
<style>
   /*****************globals*************/
   body {
   font-family: 'open sans';
   overflow-x: hidden; }
   .price span{
   text-transform: capitalize;
   }
   .preview {
   display: -webkit-box;
   display: -webkit-flex;
   display: -ms-flexbox;
   display: flex;
   -webkit-box-orient: vertical;
   -webkit-box-direction: normal;
   -webkit-flex-direction: column;
   -ms-flex-direction: column;
   flex-direction: column; }
   @media screen and (max-width: 996px) {
   .preview {
   margin-bottom: 20px; } }
   .preview-pic {
   -webkit-box-flex: 1;
   -webkit-flex-grow: 1;
   -ms-flex-positive: 1;
   flex-grow: 1; }
   .preview-thumbnail.nav-tabs {
   border: none;
   margin-top: 15px; }
   .preview-thumbnail.nav-tabs li {
   width: 18%;
   margin-right: 2.5%; }
   .preview-thumbnail.nav-tabs li img {
   max-width: 100%;
   display: block; }
   .preview-thumbnail.nav-tabs li a {
   padding: 0;
   margin: 0; }
   .preview-thumbnail.nav-tabs li:last-of-type {
   margin-right: 0; }
   .tab-content {
   overflow: hidden; }
   .tab-content img {
   width: 100%;
   -webkit-animation-name: opacity;
   animation-name: opacity;
   -webkit-animation-duration: .3s;
   animation-duration: .3s; }
   .card {
   margin-top: 50px;
   padding-bottom: 35px;
   line-height: 1.5em; }
   @media screen and (min-width: 997px) {
   .wrapper {
   display: -webkit-box;
   display: -webkit-flex;
   display: -ms-flexbox;
   display: flex; } }
   .details {
   display: -webkit-box;
   display: -webkit-flex;
   display: -ms-flexbox;
   display: flex;
   -webkit-box-orient: vertical;
   -webkit-box-direction: normal;
   -webkit-flex-direction: column;
   -ms-flex-direction: column;
   flex-direction: column; }
   .colors {
   -webkit-box-flex: 1;
   -webkit-flex-grow: 1;
   -ms-flex-positive: 1;
   flex-grow: 1; }
   .product-title, .price, .sizes, .colors {
   text-transform: UPPERCASE;
   font-weight: bold; }
   .checked, .price span {
   color: #ff9f1a; }
   .product-title, .rating, .product-description, .price, .vote, .sizes {
   margin-bottom: 15px; }
   .product-title {
   margin-top: 0; }
   .size {
   margin-right: 10px; }
   .size:first-of-type {
   margin-left: 40px; }
   .color {
   display: inline-block;
   vertical-align: middle;
   margin-right: 10px;
   height: 2em;
   width: 2em;
   border-radius: 2px; }
   .color:first-of-type {
   margin-left: 20px; }
   .add-to-cart, .like {
   background: #ff9f1a;
   padding: 1.2em 1.5em;
   border: none;
   text-transform: UPPERCASE;
   font-weight: bold;
   color: #fff;
   -webkit-transition: background .3s ease;
   transition: background .3s ease; }
   .add-to-cart:hover, .like:hover {
   background: #b36800;
   color: #fff; }
   .not-available {
   text-align: center;
   line-height: 2em; }
   .not-available:before {
   font-family: fontawesome;
   content: "\f00d";
   color: #fff; }
   .orange {
   background: #ff9f1a; }
   .green {
   background: #85ad00; }
   .blue {
   background: #0076ad; }
   .tooltip-inner {
   padding: 1.3em; }
   @-webkit-keyframes opacity {
   0% {
   opacity: 0;
   -webkit-transform: scale(3);
   transform: scale(3); }
   100% {
   opacity: 1;
   -webkit-transform: scale(1);
   transform: scale(1); } }
   @keyframes opacity {
   0% {
   opacity: 0;
   -webkit-transform: scale(3);
   transform: scale(3); }
   100% {
   opacity: 1;
   -webkit-transform: scale(1);
   transform: scale(1); } }
   /*# sourceMappingURL=style.css.map */
</style>
@endsection
@section('content')
<div class="container">
   <div class="card">
      <div class="container-fliud">
         <div class="wrapper row">
            <div class="preview col-md-6">
               <div class="preview-pic tab-content">
                  @foreach($packageweb->package_web_images as $key => $item)
                  {{-- @for($i=0; $i < count($item->image->path); $i++) --}}
                  {{-- {{dd($i)}} --}}
                  <div class="tab-pane @if($key== 0){{'active'}}@endif" id="pic-{{$item->id}}">
                     <img src="{{Storage::url($item->image->path)}}" />
                  </div>
                  @endforeach
               </div>
               <ul class="preview-thumbnail nav nav-tabs">
                  @foreach($packageweb->package_web_images as $key => $item)
                  <li class="@if($key== 0){{'active'}} @endif">
                     <a data-target="#pic-{{$item->id}}" data-toggle="tab">
                     <img src="{{Storage::url($item->image->path)}}" />
                     </a>
                  </li>
                  @endforeach 
               </ul>
            </div>
            <div class="details col-md-6">
               <h3 class="product-title">{{$packageweb->title}}</h3>
               {{-- 
               <div class="rating">
                  <div class="stars">
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star"></span>
                     <span class="fa fa-star"></span>
                  </div>
                  <span class="review-no">41 reviews</span>
               </div>
               --}}
               <p class="product-description">{{$packageweb->description}}</p>
               <h4 class="price">current price: <span>Rp. {{number_format($packageweb->currentprice,0,',','.') }}/ Years</span></h4>
               <p><strong>Detail</strong><br> {{$packageweb->detail}}</p>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('js')
@endsection