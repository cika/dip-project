@extends('layouts.app')
@section('content')
<div id="fh5co-container">
   <div class="container">
      <div class="row">
         <div class="col-md-8">
            <h1 style="margin-bottom: 10%; margin-top: 10px;">DIP Package Tracking</h1>
            <p>Track Your Project, Please enter Code Booking Number (One Per Line) Then klik Track Project</p>
            <div class='row'>
               <div class='col-md-12 col-centered'>
                  <form id="tracking-form"  method="POST">
                     <div class='form-group'>
                        <div class='input-group input-group-lg'>
                           <input class='form-control' placeholder='Enter tracking number' type='text' name="search" id="search">
                           <span class='input-group-btn'>
                              <button class='btn btn-default btn-parcels' type='submit' id="btnSearch">
                                 <div class='fa fa-binoculars'></div>
                                 <span class='hidden-xs'>
                                 Track Project
                                 </span>
                              </button>
                           </span>
                        </div>
                     </div>
                  </form>
                  <table hidden>
                     <thead>
                        <tr>
                           <th>Last Update</th>
                           <th>Email</th>
                           <th>Status</th>
                        </tr>
                     </thead>
                     <tbody></tbody>
                  </table>
               </div>
            </div>
         </div>
         <div class="col-md-4">
            <a class="screenshot-link"><img class="img-responsive" src="{{asset('images/tracking.png')}}" alt="Hero ios android" />
            </a>
         </div>
      </div>
      <div id="fh5co-about" style="padding-top: 2em !important;">
         <div class="container">
            <div class="row animate-box">
               <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                  <h2>Step Tracking</h2>
               </div>
            </div>
            <div class="row">
               <div class="col-md-2 col-sm-2 col-md-offset-1 text-center animate-box" data-animate-effect="fadeIn">
                  <div class="fh5co-step">
                     <img class="img image-trace" src="{{asset('images/step/product-receive.jpg')}}">
                     <h3>1</h3>
                     <strong class="role">PRODUCT INFO (PI) RECEIVED</strong>
                  </div>
               </div>
               <div class="col-md-2 col-sm-2 text-center animate-box" data-animate-effect="fadeIn">
                  <div class="fh5co-step">
                     <img class="img image-trace" src="{{asset('images/step/sample-arrived.jpg')}}">
                     <h3>2</h3>
                     <strong class="role">SAMPLE ARRIVED</strong>
                  </div>
               </div>
               <div class="col-md-2 col-sm-2 text-center animate-box" data-animate-effect="fadeIn">
                  <div class="fh5co-step">
                     <img class="img image-trace" src="{{asset('images/step/photo-session.jpg')}}">
                     <h3>3</h3>
                     <strong class="role">PHOTO SESSION</strong>
                  </div>
               </div>
               <div class="col-md-2 col-sm-2 text-center animate-box" data-animate-effect="fadeIn">
                  <div class="fh5co-step">
                     <img class="img image-trace" src="{{asset('images/step/editing-image.jpg')}}">
                     <h3>4</h3>
                     <strong class="role">IMAGE EDITING</strong>
                  </div>
               </div>
               <div class="col-md-2 col-sm-2 text-center animate-box" data-animate-effect="fadeIn">
                  <div class="fh5co-step">
                     <img class="img image-trace" src="{{asset('images/step/image-send.jpg')}}">
                     <h3>5</h3>
                     <strong class="role">IMAGES SEND TO CLIENTS</strong>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('js')
<script>
   jQuery(document).ready(function($) {
         $(document).on('submit','#tracking-form',function(event) {
             event.preventDefault();
             $('.fa-binoculars').hide();
             $('.btn-default').append(" <div class='fa fa-spinner fa-spin'></div>");
             $('table tbody').empty();
             var data = $('#tracking-form').serialize();
             $.ajax({
                 url: '/trace',
                 type: 'POST',
                 data : data,
                 cache : false,
                 success: function(data){
                    if (data.success){
                       $('table').show();
                       if(data.data != null){
                           $('table tbody').append("<tr><td>"+data.data.updated_at+"</td><td>"+data.data.email+"</td><td style='color:red'><b>"+data.data.status+"</b></td></tr>");  
                           $('.fa-spin').hide();   
                           $('.fa-binoculars').show();
                       } else {
                           $('table tbody').append("<tr><td colspan='3'>Data Tidak di Temukan</td></tr>");
                           $('.fa-spin').hide();   
                           $('.fa-binoculars').show();
                       }
                    }
                 },
                 error: function(response){
                   toastr.error('Whoops Something Error')
                 }
             })
         });
   });
</script>
@endsection
