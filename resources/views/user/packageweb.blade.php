@extends('layouts.app')
@section('css')
<style>
   .columns {
   float: left;
   width: 33.3%;
   padding: 8px;
   }
   .price {
   list-style-type: none;
   border: 1px solid #eee;
   margin: 0;
   padding: 0;
   -webkit-transition: 0.3s;
   transition: 0.3s;
   box-shadow: 0 8px 12px 0 rgba(0,0,0,0.2)
   }
   .price .header {
   background-color: #111;
   color: white;
   font-size: 25px;
   }
   .price li {
   border-bottom: 1px solid #eee;
   padding: 20px;
   text-align: center;
   }
   .price .grey {
   background-color: #eee;
   font-size: 20px;
   }
   .button {
   background-color: #ff8c00;
   border: none;
   color: white;
   padding: 10px 25px;
   text-align: center;
   text-decoration: none;
   font-size: 18px;
   }
   .accordion a {
   position: relative;
   display: -webkit-box;
   display: -webkit-flex;
   display: -ms-flexbox;
   display: flex;
   -webkit-box-orient: vertical;
   -webkit-box-direction: normal;
   -webkit-flex-direction: column;
   -ms-flex-direction: column;
   flex-direction: column;
   width: 100%;
   padding: 1rem 3rem 1rem 1rem;
   font-size:20px;
   font-weight: 400;
   border-bottom: 1px solid #e5e5e5;
   }
   .accordion a:hover,
   .accordion a:hover::after {
   cursor: pointer;
   color: #03b5d2;
   }
   .accordion a:hover::after {
   border: 1px solid #03b5d2;
   }
   .accordion a.active {
   color: #03b5d2;
   border-bottom: 1px solid #03b5d2;
   }
   .accordion a::after {
   content: '\002B';
   position: absolute;
   float: right;
   right: 1rem;
   font-size: 1rem;
   color: #7288a2;
   padding: 5px;
   width: 30px;
   height: 30px;
   -webkit-border-radius: 50%;
   -moz-border-radius: 50%;
   border-radius: 50%;
   border: 1px solid #7288a2;
   text-align: center;
   }
   .accordion a.active::after {
   content: "\2212";
   color: #03b5d2;
   border: 1px solid #03b5d2;
   }
   .accordion .content {
   opacity: 0;
   padding: 0 1rem;
   max-height: 0;
   border-bottom: 1px solid #e5e5e5;
   overflow: hidden;
   clear: both;
   -webkit-transition: all 0.2s ease 0.15s;
   -o-transition: all 0.2s ease 0.15s;
   transition: all 0.2s ease 0.15s;
   }
   .accordion .content p {
   font-size: 15px;
   font-weight: 300;
   }
   .accordion .content.active {
   opacity: 1;
   padding: 1rem;
   max-height: 100%;
   -webkit-transition: all 0.35s ease 0.15s;
   -o-transition: all 0.35s ease 0.15s;
   transition: all 0.35s ease 0.15s;
   }
   .faq {
   margin: 0 auto;
   padding: 4rem;
   }

   .step{
    font-weight: bold;
    text-transform: uppercase;
    font-size: 17px;
   }
   .caption-step{
    color: #ff8c00;
   }
   .detail-step{
    text-transform: none;
    font-size: 14px;
   }
   @media only screen and (max-width: 600px) {
   .columns {
   width: 100%;
   }
   }
</style>
@endsection
@section('content')
<div id="fh5co-container">
   <h2 style="text-align:center; margin-top: 25px;"> Choose Your Packages Website</h2>
   <div class="container">
      @foreach($packageweb as $item)
      <div class="columns">
         <ul class="price">
            <li class="header">{{$item->title}}</li>
            <li class="grey"><span style="text-decoration: line-through;font-size:16px;">Rp.{{number_format($item->oldprice,0,',','.')}} / Years</span><br>Rp. {{number_format($item->currentprice,0,',','.')}} / Years</li>
            @foreach(explode(',', $item->paket) as $text)
            <li>{{$text}}</li>
            @endforeach()
            <li class="grey"><a href="{{route('packages_web_detail',['id'=>$item->id, 'name'=> str_replace(' ','-',$item->title)])}}" class="button">Detail</a></li>
         </ul>
      </div>
      @endforeach
   </div>
   <div class="container">
      <section>
         <h3 class="" style="text-align: center;">How We Work</h3>
         <div class="">
            <span style=""></span>
         </div>
         <div class="row">
            <div class="col-md-3">
               <div class="step" style="text-align: center;">
                  <i class="fa fa-lightbulb-o fa-4x" aria-hidden="true"></i>
                  <h5 class="step">Step 1</h5>
                  <p class="caption-step">SHARE YOUR VALUE</p>
                  <p class="detail-step">Tell the ideas and values ​​that you want to put on the website</p>
               </div>
            </div>
            <div class="col-md-3">
               <div class="step" style="text-align: center;">
                  <i class="fa fa-comments fa-4x" aria-hidden="true"></i>
                  <h5 class="step">Step 2</h5>
                  <p class="caption-step">BRAINSTORMING</p>
                  <p class="detail-step">DIP provide design recommendations and features that suit your needs</p>
               </div>
            </div>
            <div class="col-md-3">
               <div class="step" style="text-align: center;">
                  <i class="fa fa-eye fa-4x" aria-hidden="true"></i>
                  <h5 class="step">Step 3</h5>
                  <p class="caption-step">PROTOTYPE</p>
                  <p class="detail-step">DIP make a prototype to give you a user experience</p>
               </div>
            </div>
            <div class="col-md-3">
               <div class="step" style="text-align: center;">
                  <i class="fa fa-handshake-o fa-4x" aria-hidden="true"></i>
                  <h5 class="step">Step 4</h5>
                  <p class="caption-step">FIRST PAYMENT</p>
                  <p class="detail-step">You make an down payment as a receipt</p>
               </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3">
               <div class="step" style="text-align: center;">
                  <i class="fa fa-cubes fa-4x" aria-hidden="true"></i>
                  <h5 class="step">Step 5</h5>
                  <p class="caption-step">WORK IN PROGRESS</p>
                  <p class="detail-step">DIP create a website in real terms as you wish</p>
               </div>
            </div>
            <div class="col-md-3">
               <div class="step" style="text-align: center;">
                  <i class="fa fa-history fa-4x" aria-hidden="true"></i>
                  <h5 class="step">Step 6</h5>
                  <p class="caption-step">FEEDBACK</p>
                  <p class="detail-step">You provide a review of the website that has been made, the Imaginary will provide a revision if needed</p>
               </div>
            </div>
            <div class="col-md-3">
               <div class="step" style="text-align: center;">
                  <i class="fa fa-thumbs-up fa-4x" aria-hidden="true"></i>
                  <h5 class="step">Step 7</h5>
                  <p class="caption-step">SECOND PAYMENT</p>
                  <p class="detail-step">You pay off the rest of the payment</p>
               </div>
            </div>
            <div class="col-md-3">
               <div class="step" style="text-align: center;">
                  <i class="fa fa-smile-o fa-4x" aria-hidden="true"></i>
                  <h5 class="step">Step 8</h5>
                  <p class="caption-step">ENJOY YOUR WEBSITE</p>
                  <p class="detail-step">DIP give you full website access</p>
               </div>
            </div>
         </div>
      </section>
  </div>
      <section class="cms" style="background-color: #fff; padding-top: 20px;padding-bottom: 20px;">
         <div class="row">
            <div class="col-md-6">
               <img class="img img-responsive" src="{{asset('images/dashboard-wordpress.png')}}">
            </div>
            <div class="col-md-6">
               <h3>Easy Management of Website Content with CMS</h3>
               <p>Content Management System (CMS) makes websites easier to manage online. We use WordPress as our website creation platform, Worpdress is known for its strength and flexibility in creating blog engines for websites for large companies.</p>
               <p>The following are some of the advantages of the WordPress CMS:</p>
               <ul>
                  <li>SEO Friendly</li>
                  <li>Easy content management</li>
                  <li>Easy Editor</li>
                  <li>Secure, Core WP always Update</li>
               </ul>
            </div>
         </div>
      </section>
      <section class="faq">
         <h2>Some Frequently Asked Questions</h2>
         <div class="accordion">
            <div class="accordion-item">
               <a>What data needs to be prepared?</a>
               <div class="content">
                  <p>Yes, we need some of your data as material to build a website related to the information you want to display on your site. Among others: company profile / business profile, logo, product description, product photos / images, and articles needed.</p>
               </div>
            </div>
            <div class="accordion-item">
               <a>What if the data isn't ready?</a>
               <div class="content">
                  <p>Don't worry, we understand your busy life. Therefore, you can give it to us gradually. Once all the data has been collected, we will help you do the content so that the data can be uploaded on the website.</p>
               </div>
            </div>
            <div class="accordion-item">
               <a>How do I add content?</a>
               <div class="content">
                  <p>With a few clicks, you can add posts and upload content on the WordPress Dashboard admin panel page Or Admin Panel Framework. This panel provides a complete content processing feature.</p>
               </div>
            </div>
            <div class="accordion-item">
               <a>Is there a warranty?</a>
               <div class="content">
                  <p>We grow when your business is successful. We have included support in each package for making our website, and we are ready to help to make improvements when a website "error".</p>
               </div>
            </div>
         </div>
      </section>
   </div>
</div>
@endsection
@section('js')
<script src="https://unpkg.com/ionicons@4.2.2/dist/ionicons.js"></script>
<script>
   jQuery(document).ready(function($) {
   const items = document.querySelectorAll(".accordion a");
   
   function toggleAccordion(){
   this.classList.toggle('active');
   this.nextElementSibling.classList.toggle('active');
   }
   
   items.forEach(item => item.addEventListener('click', toggleAccordion));
   });
</script>
@endsection