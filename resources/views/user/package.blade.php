@extends('layouts.app')

@section('content')
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2 style="margin-top: 10px;">Packages</h2>
					<p>Digital Session Fees include the time and talent of the photographer and a selection of edited digital images as listed in the description.</p>
				</div>
			</div>
			<div class="row">
				@foreach($packages as $item)
				<div class="col-lg-4 col-md-4">
					<div class="fh5co-blog animate-box">
						<img class="img-responsive" style="height: 230px; width: 100%;" src="{{Storage::url($item->package_images[0]->image->path)}}" alt="">
						<div class="blog-text">
							<span class="posted_on" style="text-transform: capitalize;">{{$item->type}}</span>
							<span class="comment"><a href="">{{$item->frame}}<i class="fa fa-camera"> frame</i></a></span>
							<h3><a href="#">{{$item->title}}</a></h3>
							<div class="box-description">
							<p class="package-description">{{$item->description}}</p>
							</div>
							<a href="{{route('packages_detail',['id'=>$item->id, 'name'=> str_replace(' ','-',$item->title)])}}" class="btn btn-primary">Read More</a>
						</div> 
					</div>
				</div>
				@endforeach
			</div>
		</div>
@endsection
@section('js')
@endsection