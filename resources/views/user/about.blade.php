@extends('layouts.app')
@section('content')
<div id="fh5co-container">
   <div class="container">
      <div class="row">
         <div class="wow fadeInUp col-md-4 col-sm-5">
			@foreach($about1 as $item)
			<?php echo($item->content) ?>
			@endforeach
		</div>
		<div class="wow fadeInUp col-md-7 col-sm-7">
			@foreach($about2 as $item)
			<?php echo($item->content) ?>
			@endforeach
		</div>
		<div class="clearfix"></div>
			@foreach($about3 as $item)
			<?php echo($item->content) ?>
			@endforeach
      </div>
   </div>
</div>
@endsection