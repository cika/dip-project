@extends('layouts.app')
@section('css')
<style>
   .term{
   list-style-type: inherit;
   font-size: 14px;
   }
   .conditions{
   list-style-type: inline;
   font-size: 18px;
   font-weight: bold;
   }
   .p-term {    
   line-height: normal;    
   letter-spacing: normal;
   font-size: 14px;
   }
   .p-termcondition{
   margin-top: 20px;
   }
</style>
@endsection
@section('content')
<div id="fh5co-contact">
   <div class="container">
      <h3>Kebijakan Privasi</h3>
      <div class="p-termcondition">
         <p class="p-term">Adanya Kebijakan Privasi ini adalah komitmen nyata dari DIP-Project untuk menghargai dan melindungi setiap data atau informasi pribadi Pengguna situs www.DIP-Project.id, situs-situs turunannya, serta aplikasi gawai DIP-Project (selanjutnya disebut sebagai "Situs").</p>
         <p class="p-term">Kebijakan Privasi ini (beserta syarat-syarat penggunaan dari situs DIP-Project sebagaimana tercantum dalam "Syarat & Ketentuan" dan informasi lain yang tercantum di Situs) menetapkan dasar atas perolehan, pengumpulan, pengolahan, penganalisisan, penampilan, pembukaan, dan/atau segala bentuk pengelolaan yang terkait dengan data atau informasi yang Pengguna berikan kepada DIP-Project atau yang DIP-Project kumpulkan dari Pengguna, termasuk data pribadi Pengguna, baik pada saat Pengguna melakukan pendaftaran di Situs, mengakses Situs, maupun mempergunakan layanan-layanan pada Situs (selanjutnya disebut sebagai "data").</p>
         <p class="p-term">Dengan mengakses dan/atau mempergunakan layanan DIP-Project, Pengguna menyatakan bahwa setiap data Pengguna merupakan data yang benar dan sah, serta Pengguna memberikan persetujuan kepada DIP-Project untuk memperoleh, mengumpulkan, menyimpan, mengelola dan mempergunakan data tersebut sebagaimana tercantum dalam Kebijakan Privasi maupun Syarat dan Ketentuan DIP-Project.</p>
      </div>
      <ul>
         <li class="conditions">A. Pengungkapan Data Pribadi Pengguna</li>
         <li class="conditions">B. Cookies</li>
         <li class="conditions">C. Penyimpanan dan Penghapusan Informasi</li>
         <li class="conditions">D. Pembaruan Kebijakan Privasi</li>
      </ul>
      <br>
      <h5>A. Pengungkapan Data Pribadi Pengguna</h5>
      <p class="p-term">
         DIP-Project menjamin tidak ada penjualan, pengalihan, distribusi atau meminjamkan data pribadi Anda kepada pihak ketiga lain, tanpa terdapat izin dari Anda, kecuali dalam hal-hal sebagai berikut:
         Dibutuhkan adanya pengungkapan data Pengguna kepada mitra 	atau pihak ketiga lain yang membantu DIP-Project dalam menyajikan layanan pada Situs dan memproses segala bentuk aktivitas Pengguna dalam Situs, termasuk memproses transaksi, verifikasi pembayaran, pengiriman produk, dan lain-lain.
      </p>
      <p class="p-term">
         DIP-Project dapat menyediakan informasi yang relevan kepada mitra usaha sesuai dengan persetujuan Pengguna untuk menggunakan layanan mitra usaha, termasuk diantaranya aplikasi atau situs lain yang telah saling mengintegrasikan API atau layanannya, atau mitra usaha yang mana DIP-Project telah bekerjasama untuk mengantarkan promosi, kontes, atau layanan yang dikhususkan	
         Dibutuhkan adanya komunikasi antara mitra usaha DIP-Project 	(seperti penyedia logistik, pembayaran, dan lain-lain) dengan 	Pengguna dalam hal penyelesaian kendala maupun hal-hal lainnya.
         DIP-Project dapat menyediakan informasi yang relevan kepada 	vendor, konsultan, mitra pemasaran, firma riset, atau penyedia layanan sejenis.
      </p>
      <p class="p-term">
         Pengguna menghubungi DIP-Project melalui media publik seperti blog, media sosial, dan fitur tertentu pada Situs, komunikasi antara 	Pengguna dan DIP-Project mungkin dapat dilihat secara publik.	
         DIP-Project dapat membagikan informasi Pengguna kepada anak perusahaan dan afiliasinya untuk membantu memberikan layanan atau melakukan pengolahan data untuk dan atas nama DIP-Project.
         DIP-Project mengungkapkan data Pengguna dalam upaya mematuhi kewajiban hukum dan/atau adanya permintaan yang sah dari aparat 	penegak hukum.
      </p>
      <h5>B. Cookies	</h5>
      <ol>
         <li class="term">Cookies adalah file kecil yang secara otomatis akan mengambil tempat di dalam perangkat Pengguna yang menjalankan fungsi dalam menyimpan preferensi maupun konfigurasi Pengguna selama mengunjungi suatu situs.</li>
         <li class="term">Cookies tersebut tidak diperuntukkan untuk digunakan pada saat melakukan akses data lain yang Pengguna miliki di perangkat 	komputer Pengguna, selain dari yang telah Pengguna setujui untuk disampaikan.</li>
         <li class="term">Walaupun secara otomatis perangkat komputer Pengguna akan 	menerima cookies, Pengguna dapat menentukan pilihan untuk melakukan 	modifikasi melalui pengaturan browser Pengguna yaitu dengan memilih untuk menolak cookies (pilihan ini dapat membatasi layanan optimal pada saat melakukan akses ke Situs).</li>
         <li class="term">DIP-Project menggunakan fitur Google Analytics Demographics and Interest. Data yang kami peroleh dari fitur tersebut, seperti umur, jenis kelamin, minat Pengguna dan lain-lain, akan kami gunakan untuk pengembangan Situs dan konten DIP-Project. Jika tidak ingin data Anda 	terlacak oleh Google Analytics, Anda dapat menggunakan Add-On Google Analytics Opt-Out Browser.</li>
         <li class="term">DIP-Project dapat menggunakan fitur-fitur yang disediakan oleh 	pihak ketiga dalam rangka meningkatkan layanan dan konten DIP-Project, 	termasuk diantaranya ialah penyesuaian dan penyajian iklan kepada 	setiap Pengguna berdasarkan minat atau riwayat kunjungan. Jika Anda 	tidak ingin iklan ditampilkan berdasarkan penyesuaian tersebut, maka Anda dapat mengaturnya melalui browser.</li>
      </ol>
      <h5>C. Penyimpanan dan Penghapusan Informasi</h5>
      <p class="p-term">DIP-Project akan menyimpan informasi selama akun Pengguna tetap aktif dan dapat melakukan penghapusan sesuai dengan ketentuan peraturan hukum yang berlaku.</p>
      <h5>D. Pembaruan Kebijakan Privasi</h5>
      <p class="p-term">
         DIP-Project dapat sewaktu-waktu melakukan perubahan atau pembaruan terhadap Kebijakan Privasi ini. DIP-Project menyarankan agar Pengguna membaca secara seksama dan memeriksa halaman Kebijakan Privasi ini dari waktu ke waktu untuk mengetahui perubahan apapun. Dengan tetap mengakses dan menggunakan layanan Situs maupun layanan DIP-Project lainnya, maka Pengguna dianggap menyetujui perubahan-perubahan dalam Kebijakan Privasi.
      </p>
   </div>
</div>
@endsection