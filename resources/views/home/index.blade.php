@extends('layouts.app')

@section('content')
    <aside id="fh5co-hero" class="js-fullheight">
        <div class="flexslider js-fullheight">
            <ul class="slides">
            @foreach($slider as $slide)
            <li style="background-image: url({{Storage::url($slide->image->path)}});">
                <div class="overlay-gradient"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 text-center js-fullheight slider-text">
                            <div class="slider-text-inner">
                                <h1>{{$slide->title}}</h1>
                                    <h2>{{$slide->description}}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            @endforeach
            </ul>
        </div>
    </aside>

    <div id="fh5co-practice" class="fh5co-bg-section">
        <div class="container">
            <div class="row animate-box">
                <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                    <h2>Other Services</h2>
                    <p>Brand Consulting</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 text-center animate-box">
                    <div class="services">
                        <span class="icon">
                            <i class="fa fa-file-text-o"></i>
                        </span>
                        <div class="desc">
                            <h3>Channel Management</h3>
                            <p>Manage your product from Inventory, upload process, Packaging, and delivery</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 text-center animate-box">
                    <div class="services">
                        <span class="icon">
                            <i class="fa fa-code"></i>
                        </span>
                        <div class="desc">
                            <h3>Web Design & Development</h3>
                            <p>Provide A-Z for specific brand website development, web maintainance, SEO optimization.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 text-center animate-box">
                    <div class="services">
                        <span class="icon">
                            <i class="fa fa-instagram"></i>
                        </span>
                        <div class="desc">
                            <h3>Social Media Management</h3>
                            <p>Manage your brand through social media</p>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-md-3 text-center animate-box">
                    <div class="services">
                        <span class="icon">
                            <i class="fa fa-envelope-o"></i>
                        </span>
                        <div class="desc">
                            <h3>Newsletter</h3>
                            <p>Create, manage, strategical plan and analyze newsletter managament through third party service (mailchimp, emarsys, weblite).</p>
                        </div>
                    </div>
                </div> -->
                <div class="col-md-3 text-center animate-box">
                    <div class="services">
                        <span class="icon">
                            <i class="fa fa-camera"></i>
                        </span>
                        <div class="desc">
                            <h3>Mobile Studio</h3>
                            <p>Provide team to be placed on clients site.(inc : studio set, stylish, MUA, and photographer)</p>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <div id="fh5co-started" style="background-image:url({{asset('images/img_bg_2.jpg')}});">
        <div class="overlay"></div>
        <div class="container">
            <div class="row animate-box">
                <div class="col-md-8">
                    <h2 style="color:#fff;">NEED HELP?</h2>
                    <p style="font-size: 35px;"> CALL/WHATSAPP/ WE SUPPORT <br> <b>+62 877-6611-6931
                    </b></p>
                </div>
                <div class="col-md-2">
                    <img class="img img-responsive"  src="{{asset('images/live-chat.png')}}">
                </div>
            </div>

        </div>
    </div>

    <div id="fh5co-content" style="margin-top: 80px;">
        <div class="fh5co-video video">
            <video height="470" controls autoplay muted loop>
                  <source src="{{asset('video/campaign.mov')}}" >
            </video>
            <div class="overlay"></div>
        </div>
        <div class="choose animate-box">
            <div class="fh5co-heading">
                <h2>One Stop E-Commerce Solutions</h2>
                <p>Dip-Project is Indonesia’s latest concept focusing on assisting local Indonesian brands,  ranging from small medium enterprise to local well established retailers, to enter e-commerce business</p>

                <p>Founded by the team who has been executing the expansion of major ecommerce player in Indonesia, Premisize are committed to provide an international standard quality of product imaging for our clients</p>
            </div>
            <!-- <div class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%">
                Adoption Law 50%
                </div>
            </div>
            <div class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:80%">
                Family Law 80%
                </div>
            </div>
            <div class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:70%">
                Real Estate Law 70%
                </div>
            </div>
            <div class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:40%">
                Personal Injury Law 40%
                </div>
            </div> -->
        </div>
    </div>


    <div id="fh5co-project">
        <div class="container">
            <div class="row animate-box">
                <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                    <h2>OUR CLIENTS</h2>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center fh5co-project animate-box">
                    <img class="img img-responsive client-our" src="{{asset('images/client.jpg')}}" alt="Free HTML5 Website Template by dip-project">
                </div>
            </div>
        </div>
    </div>

@endsection


