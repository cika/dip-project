<?php
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Debug\Dumper;
use Illuminate\Support\Facades\Log;

if (!function_exists('random_alpha_numeric')) {
    /**
     * Generate random alphanumeric.
     *
     * @param  integer $length
     *
     * @return string
     */
    function random_alpha_numeric($length)
    {
        $pool = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
    }
}

if (!function_exists('setActive')) {
    
        function setActive(string $path, string $class_name = "is-active")
        {
            return Request::path() === $path ? $class_name : "";
        }

}
