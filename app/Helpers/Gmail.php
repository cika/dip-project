<?php


namespace App\Helpers;


use \Sunra\PhpSimple\HtmlDomParser;
use \PHPMailer\PHPMailer\PHPMailer;

class Gmail extends PHPMailer
{

	/**
     * @var Gmail
     */
    private static $INSTANCE;

    /**
     * @var \Google_Client
     */
    protected $client;
	
	function __construct()
	{
		$this->client = $this->getClient();
	}


	public static function getInstance()
	{
		if (self::$INSTANCE == null)
            self::$INSTANCE = new Gmail();

        return self::$INSTANCE;
	}

	/**
     * Returns an authorized API client.
     * @return \Google_Client the authorized client object
     */
    private function getClient()
    {
        $client = new \Google_Client();
        $client->setApplicationName("DIP-Project");
        $client->setScopes(\Google_Service_Gmail::GMAIL_COMPOSE);
        $client->setAuthConfig(base_path('credentials.json'));
        $client->setAccessType('offline');
        // $client->setPrompt('select_account consent');

        // Load previously authorized token from a file, if it exists.
        // The file token.json stores the user's access and refresh tokens, and is
        // created automatically when the authorization flow completes for the first
        // time.
        $tokenPath = base_path('token.json');
        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);
        }

        // If there is no previous token or it's expired.
        if ($client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print 'Enter verification code: ';
                define('STDIN', fopen("php://stdin", "r"));
                $authCode = trim(fgets(STDIN));

                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                $client->setAccessToken($accessToken);

                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new \Exception(join(', ', $accessToken));
                }
            }
            // Save the token to a file.
            if (!file_exists(dirname($tokenPath))) {
                mkdir(dirname($tokenPath), 0700, true);
            }
            file_put_contents($tokenPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }

    public function sendMail($from, $name,  $to, $subject, $message)
    {
    	$gmail = new \Google_Service_Gmail($this->client);

        $this->isSMTP();
        $this->SMTPDebug = 0;
        //Recipients
        $this->setFrom($from, $name);
        $this->addAddress($to);  

        //Content
        $this->isHTML(true); 
        $this->attachmentImage($message);
        $this->Subject = $subject;
        $this->Body = $message;

        if ($this->preSend()) {
            try {
                // The message needs to be encoded in Base64URL
                $mime = rtrim(strtr(base64_encode($this->MIMEHeader. $this->MIMEBody), '+/', '-_'), '=');
                $msg = new \Google_Service_Gmail_Message();
                $msg->setRaw($mime);
                $objSentMsg = $gmail->users_messages->send("me", $msg);
        
         
            } catch (\Exception $e) {
                print($e->getMessage());
            }
        }

        return false;
    }

    private function attachmentImage(&$message)
    {
        $message  = HtmlDomParser::str_get_html($message);
        $imagesource = $message->find('img');
        $filesystem = new \Illuminate\Filesystem\Filesystem();
        foreach ($imagesource as &$item) {
            $filepath = public_path($item->src);
            $item->src = "cid:" . $filesystem->basename($filepath);
            $this->addEmbeddedImage($filepath, $filesystem->basename($filepath), $filesystem->basename($filepath));
        }
    }

}