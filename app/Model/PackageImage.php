<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PackageImage extends Model
{
   public function image()
    {
        return $this->belongsTo('App\Model\Image');
    }

   public function package()
   {
   	   return $this->belongsTo('App\Model\Package');
   }
}
