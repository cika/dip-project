<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tracking extends Model
{

	const STATUS_RECEIVED      = "Received";
	const STATUS_SAMPLE_ARRIVED = "Arrived";
	const STATUS_PHOTO_SESSION  = "Process";
	const STATUS_IMAGE_EDITING  = "Editing";
	const STATUS_IMAGE_SEND_CLIENT = "Done";
	const STATUS_CANCEL = "Cancel";
	const STATUS =[self::STATUS_RECEIVED,self::STATUS_SAMPLE_ARRIVED,self::STATUS_PHOTO_SESSION,self::STATUS_IMAGE_EDITING,self::STATUS_IMAGE_SEND_CLIENT];


	protected $fillable = [   	
    	'email','code','status'
    ];
 
}
