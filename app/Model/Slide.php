<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $fillable = [   	
    	'title','description'
    ];

    /**
     * Get the image that owns the slide.
     */

    public function image()
    {
        return $this->belongsTo('App\Model\Image','image_id','id');
    }

}
