<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PackageWeb extends Model
{
    protected $table = 'package_web';

	protected $fillable = [
		'title','paket','description','detail','oldprice','currentprice'
	];	

	public function package_web_images()
	{
		return $this->hasMany('App\Model\PackageWebImage', 'package_web_id', 'id');
	}
}
