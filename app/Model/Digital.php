<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Digital extends Model
{

	protected $table = 'digitals';
 	protected $fillable  = [ 
     	'name','content'	
    ];
}
