<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PackageWebImage extends Model
{

   protected $table = 'packages_web_images';
   protected $fillable = ['package_web_id', 'image_id'];
   
   public function image()
    {
        return $this->belongsTo('App\Model\Image');
    }

   public function package_web()
   {
   	   return $this->belongsTo('App\Model\PackageWeb');
   }

}
