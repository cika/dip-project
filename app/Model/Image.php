<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
  	protected $fillable = [
    	'filename', 'title', 'path', 
    	'extension', 'size'
    ];

    public function slides()
    {
        return $this->hasMany('App\Model\Slide');
    }

    public function packages()
    {
        return $this->hasMany('App\Model\Package');
    }

    public function webs()
    {
        return $this->hasMany('App\Model\Web');
    }

}
