<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\Gmail;

class GmailTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gmail:run {option}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To test gmail fuctionalities';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->argument('option') === "login") {
            $this->login();
        }else if ($this->argument('option') === "sendMail") {
            $this->sendMail();
        }else{
            $this->info(sprintf('Option "%s" is not available', $this->argument('option')));
        }
    }

    private function login()
    {
        $client = Gmail::getInstance();
    }

    public function sendMail()
    {
        $tracking = \App\Model\Tracking::find(1);
        $mail = new \App\Mail\DoneTracking($tracking);
        Gmail::getInstance()->sendMail(env('MAIL_USERNAME'), 'Dip-Project', $tracking->email, 'Dip-Project', $mail->render());
    }
}
