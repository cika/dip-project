<?php

namespace App\Http\Controllers;
use App\Model\Slide;
use App\Model\Image;
use App\Model\Digital;
use App\Model\Contact;
use App\Model\Tracking;
use App\Model\Package;
use App\Model\PackageImage;
use App\Model\ContactUS;
use App\Helpers\Gmail;
use App\Model\PackageWeb;
use App\Model\AboutUs;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $slider = Slide::with('image')->get();
        // dd($slider);
        return view('home.index',['slider' =>$slider]);
    }

    public function digital()
    {
        $digital = Digital::where("name", "digital")->first();
        return view('user.digital', ['digital'=> $digital]);
    }

    public function contact()
    {
        $contact = Contact::all();
        return view('user.contact', ['contact'=>$contact]);
    }

    public function contactUSPost(Request $request)
    {
         $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ]);
       // ContactUS::create($request->all());
        // dd($request->message);
       $contact = New ContactUS();
       $contact->name = $request->name;
       $contact->email = $request->email;
       $contact->subject = $request->subject;
       $contact->message = $request->message;
       $contact->save();
       

       $mail = new \App\Mail\ContactUs($contact);
       Gmail::getInstance()->sendMail(env('MAIL_USERNAME'), 'User-Feedback', env('MAIL_USERNAME'), 'User-Feedback', $mail->render());

       return response()->json([
                'success' => true,
                'message'   => 'Thanks for contacting us!'
        ]); 
       
    }

    public function trace(Request $request)
    {
        if($request->isMethod('post')){

            $search = $request->search;

            $data =  Tracking::where(function ($query) use($search){
                $query->where('code', '=', "$search");
            })->first();

            return response()->json([
                'data' => $data,
                'success' => true,
                'message'   => 'Tracking Found'
            ]); 

        }

        return view('user.tracking');
    }

    public function package()
    {
        $packages = Package::all();
        
        return view('user.package',['packages'=>$packages]);
    }

    public function package_detail($id)
    {
        $package = Package::find($id);
        return view('user.detailpackage',['package' =>$package]);
    }

    public function package_web()
    {
        $packageweb = PackageWeb::all();
        
        return view('user.packageweb',['packageweb'=>$packageweb ]);
    }

    public function package_web_detail($id)
    {
        $packageweb = PackageWeb::find($id);
        return view('user.detailpackageweb',['packageweb' =>$packageweb]);
    }

      public function about()
    {
        $about1 = AboutUs::where("name", "line1")->get();
        $about2 = AboutUs::where("name", "line2")->get();
        $about3 = AboutUs::where("name", "line3")->get();
        return view('user.about', ['about1'=> $about1,'about2'=> $about2, 'about3'=> $about3]);

    }

    public function terms()
    {
        return view('user.terms');

    }

    public function privacypolicy()
    {
        return view('user.privacypolicy');
    } 
}
