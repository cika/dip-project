<?php

namespace App\Http\Controllers\Admin;
use App\Model\Admin;
use App\Model\Tracking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {

        $received = Tracking::where('status', 'Received')->count();
        $arrived = Tracking::where('status', 'Arrived')->count();
        $process = Tracking::where('status', 'Process')->count();
        $editing = Tracking::where('status', 'Editing')->count();
        $done = Tracking::where('status', 'Done')->count();
    	return view('admin.home.dashboard',['received' => $received,'arrived' => $arrived,'process' => $process,'editing' => $editing,'done' => $done ]);
    }

    public function profile($id)
    {
        $id = intval($id);

        return view('admin.profile.index')
                ->with('user', Admin::find($id)->first());
    }

    public function googleanalytics(Request $request)
    {
        return view('admin.home.googleanalytics');
    }

   
    public function setting(Request $request)
    {
        $id = intval($request->id);

        $validator = $request->validate([
            'name'  => 'nullable|string|max:191',
        ]);

        $user = Admin::find($id);
        $user->name  = $request->name;
        $user->save();

        return response()->json([
            'success' => true,
        ]);
    }

    public function password(Request $request)
    {
        $id = intval($request->id);
        $user = Admin::find($id);

        if( !(Hash::check($request->current_password, $user->password)) ){
            return response()->json([
                'success' => false,
                'message' => 'Your current password does not matches with the password you provided. Please try again.',
            ]);
        }

        $validator = $request->validate([
            'new_password'         => 'required|min:6',
            'new_password_confirm' => 'required_with:new_password|same:new_password|min:6',
        ]);

        $user->password = Hash::make($request->new_password);
        $user->save();

        return response()->json([
            'success' => true,
        ]);
    }

}
