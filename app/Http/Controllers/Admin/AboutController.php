<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Model\AboutUs;

class AboutController extends Controller
{
    public function index(Request $request)
    {
        if ($request->isMethod('POST')) {
        // dd($request->all());
            AboutUs::updateOrCreate(['name' => 'line1'],['content' => $request->line1]);
            AboutUs::updateOrCreate(['name' => 'line2'],['content' => $request->line2]);
            AboutUs::updateOrCreate(['name' => 'line3'],['content' => $request->line3]);

            return response()->json([
                'success' => true,
                'message' => 'Data Successfully Changed'
            ]);

        }

        $about = AboutUs::all();
        return view('admin.about.index', ['about' => $about]);
    }
}
