<?php

namespace App\Http\Controllers\Admin;
use App\Model\Tracking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Gmail;

class TrackingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       if( $request->isMethod('post') ){
            $search;
            $start = $request->start;
            $length = $request->length;

            if( !empty($request->search) )
                $search = $request->search['value'];
            else
                $search = null;

            $column = [
                "email","code",'status','updated_at'
            ];
            $query = Tracking::where(function ($query) use ($search){
                        $query->where("email", 'LIKE', "%$search%")
                            ->orWhere("code", 'LIKE', "%$search%")
                            ->orWhere("updated_at", 'LIKE', "%$search%")
                            ->orWhere("status", 'LIKE', "%$search%");
                    })
                    ->where("status", '!=', Tracking::STATUS_CANCEL);

            $total = $query->count();

            $data = $query->orderBy($column[$request->order[0]['column'] - 1], $request->order[0]['dir'])
                    ->orderBy('created_at','desc')
                    ->skip($start)
                    ->take($length)
                    ->get();

            $response = [
                'data'  =>  $data,
                'draw' => intval($request->draw),
                'recordsTotal' => $total,
                'recordsFiltered' => $total
            ];
           return response()->json($response);
       }            

        $tracking = Tracking::all();
        return view('admin.tracking.index', ['tracking' => $tracking] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $validator = $request->validate([
            'email'       => 'required|string|max:191',
            'code'    => 'nullable',
            'status'    => 'nullable',
            'note'    => 'nullable',
        ]);

        if( $request->isMethod('POST') ){
            $tracking = new Tracking();
            $tracking->email = $request->email;
            $tracking->code = random_alpha_numeric(6);
            $tracking->status = Tracking::STATUS_RECEIVED;
            $mail = new \App\Mail\TrackingMail($tracking);
            Gmail::getInstance()->sendMail(env('MAIL_USERNAME'), 'Dip-Project', $tracking->email, 'Dip-Project', $mail->render());
            $tracking->note = $request->note;
            $tracking->save();

            return response()->json([
                'success' => true,
                'message'   => 'Tracking Successfully Added'
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $request->validate([
            'email'       => 'nullable',
            'code'    => 'nullable',
            'note'    => 'nullable',
        ]);

        $tracking       =Tracking::find($id);
        $tracking->note = $request->note;
        $tracking->save();

        return response()->json([
            'success' => true,
            'message'   => 'Tracking Successfully Edited'
        ]);

    }

    public function change(Request $request)
    {

        $tracking       =Tracking::find($request->id);
        if($tracking->status !=Tracking::STATUS_IMAGE_SEND_CLIENT){
            for ($index=0; $index < count(Tracking::STATUS); $index++) { 
                if($tracking->status === Tracking::STATUS[$index] ){
                    $tracking->status = Tracking::STATUS[$index+1];
                    if($tracking->status == Tracking::STATUS_IMAGE_SEND_CLIENT){
                        $mail = new \App\Mail\DoneTracking($tracking);
                        Gmail::getInstance()->sendMail(env('MAIL_USERNAME'), 'Dip-Project', $tracking->email, 'Dip-Project', $mail->render());
                    }
                        break;
                }   
                
            }    
        }
        $tracking->save();


        return response()->json([
            'success' => true,
            'message'   => 'Tracking Successfully Change'
        ]);

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tracking = Tracking::find($id);
        $tracking->status = Tracking::STATUS_CANCEL;
        $tracking->save();
        return response()->json([
            'success' => true,
            'message'   => 'Tracking Successfully Cancel'
        ]);
    }

    public function cancel(Request $request)
    {
       if($request->isMethod('post') ){
            $search;
            $start = $request->start;
            $length = $request->length;

            if( !empty($request->search) )
                $search = $request->search['value'];
            else
                $search = null;

            $column = [
                "email","code",'status','updated_at'
            ];

            $query = Tracking::where(function ($query) use ($search){
                        $query->where("email", 'LIKE', "%$search%")
                            ->orWhere("code", 'LIKE', "%$search%")
                            ->orWhere("updated_at", 'LIKE', "%$search%")
                            ->orWhere("status", 'LIKE', "%$search%");
                    })
                    ->where("status", Tracking::STATUS_CANCEL);

            $total = $query->count();

            $data = $query->orderBy($column[$request->order[0]['column'] - 1], $request->order[0]['dir'])
                    ->orderBy('created_at','desc')
                    ->skip($start)
                    ->take($length)
                    ->get();

            $response = [
                'data'  =>  $data,
                'draw' => intval($request->draw),
                'recordsTotal' => $total,
                'recordsFiltered' => $total
            ];
           return response()->json($response);
       }

       return view('admin.tracking.cancel');
    }
}
