<?php

namespace App\Http\Controllers\Admin;
use App\Model\Image;
use App\Model\Slide;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if( $request->isMethod('post') ){
            $search;
            $start = $request->start;
            $length = $request->length;

            if( !empty($request->search) )
                $search = $request->search['value'];
            else
                $search = null;

            $column = [
                "title",
            ];

            $total = Slide::with(['image'])
                    ->where("title", 'LIKE', "%$search%")
                    ->count();

            $data = Slide::with(['image'])
                    ->where("title", 'LIKE', "%$search%")
                    ->orderBy($column[$request->order[0]['column'] - 1], $request->order[0]['dir'])
                    ->skip($start)
                    ->take($length)
                    ->get();

            $response = [
                'data'  =>  $data,
                'draw' => intval($request->draw),
                'recordsTotal' => $total,
                'recordsFiltered' => $total
            ];

           return response()->json($response);
       }
        return view('admin.slide.index');
    }


    public function store(Request $request)
    {
        $validator = $request->validate([
            'title'       => 'required|string|max:191',
            'image_id'    => 'bail|required|mimes:jpeg,jpg,png|max:6000',
        ]);

        $slide          = new Slide();
        $slide->title   = $request->title;
        $slide->description   = $request->description;

        if( $request->image_id != null ){
            $filename  = $request->file('image_id')->getClientOriginalName();
            $path      = $request->file('image_id')->store('slide');
            $extension = $request->file('image_id')->getClientOriginalExtension();
            $size      = $request->file('image_id')->getClientSize();

            $image            = new Image();
            $image->filename  = time() . '_' . $filename;
            $image->title     = $request->title;
            $image->path      = $path;
            $image->extension = $extension;
            $image->size      = $size;
            $image->save();

            $slide->image()->associate($image);
        }

        $slide->save();

        if( !$slide->save() ){
            if ($request->hasFile('image_id')) {
               $imgDelete = Image::where('path', '=', $image->path);
               Storage::delete($imgDelete->path);
               $imgDelete->delete(); 
            }
        }
        else{
            return response()->json([
                'success' => true,
                'message'   => 'Slide Successfully Added'
            ]);
        }

    }


    public function update(Request $request, $id)
    {   
        $validator = $request->validate([
            'title'       => 'required|string|max:191',
            'image_id'    => 'bail|nullable|mimes:jpeg,jpg,png|max:6000',
        ]);

        $slide          = Slide::find($id);
        $slide->title   = $request->title;
        $slide->description   = $request->description;

        if( $request->image_id != null ){
            if( $slide->image != null ){
                Storage::delete($slide->image->path); 
            }

            $filename  = $request->file('image_id')->getClientOriginalName();
            $path      = $request->file('image_id')->store('slide');
            $extension = $request->file('image_id')->getClientOriginalExtension();
            $size      = $request->file('image_id')->getClientSize();
            
            
            $slide->image->filename  = time() . '_' . $filename;
            $slide->image->title     = $request->title;
            $slide->image->path      = $path;
            $slide->image->extension = $extension;
            $slide->image->size      = $size;
            $slide->image->save();
        }

        $slide->save();
     
            return response()->json([
                'success' => true,
                'message'   => 'Slide Successfully Edited'
            ]);
        
    }       

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slide = Slide::find($id);

        $image = Image::find($slide->image_id);
        if( $slide->image_id != null ){
            Storage::delete($image->path);
            $image->delete();
        }

        $slide->delete();

        return response()->json([
            'success' => true,
            'message'   => 'Slide Successfully Deleted'
        ]);
    }
}
