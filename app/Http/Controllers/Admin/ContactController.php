<?php

namespace App\Http\Controllers\Admin;
use App\Model\Contact;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if( $request->isMethod('post') ){

            $contact;
            if( count(Contact::get()) != 0 )
                $contact = Contact::find(1);
            else
                $contact = new Contact();

            $validator = $request->validate([
                'address'   => 'required|string|max:191',
                'phone'     => ['required', 'string', Rule::unique('contacts')->ignore($contact->id)],
                'email'     => ['required', 'email', Rule::unique('contacts')->ignore($contact->id)],
                'web'     => 'required|string|max:191',
            ]);

            $contact->address   = $request->address;
            $contact->phone     = $request->phone;
            $contact->email     = $request->email;
            $contact->web     = $request->web;
            $contact->save();

            return response()->json([
                'success' => true,
                'message' => 'Profile Successfully Updated',
            ]);
        }
        return view('admin.contact.index',['data' => Contact::find(1)]);
    }

}
