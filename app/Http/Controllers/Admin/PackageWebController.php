<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\PackageWeb;
use App\Model\Image;
Use App\Model\PackageWebImage;
use Illuminate\Support\Facades\Storage;

class PackageWebController extends Controller
{
    public function index(Request $request)
    {
    	if( $request->isMethod('post') ){
            $search;
            $start = $request->start;
            $length = $request->length;

            if( !empty($request->search) )
                $search = $request->search['value'];
            else
                $search = null;

            $column = [
                "title","paket","currentprice"
            ];

            $total = PackageWeb::where(function ($query) use ($search){
                        $query->where("title", 'LIKE', "%$search%")
                            ->orWhere("paket", 'LIKE', "%$search%")
                            ->orWhere("currentprice", 'LIKE', "%$search%");
                    })->count();
            $data = PackageWeb::where(function ($query) use ($search){
                        $query->where("title", 'LIKE', "%$search%")
                            ->orWhere("paket", 'LIKE', "%$search%")
                            ->orWhere("currentprice", 'LIKE', "%$search%");
                    })
                    ->orderBy($column[$request->order[0]['column'] - 1], $request->order[0]['dir'])
                    ->skip($start)
                    ->take($length)
                    ->get();

            $response = [
                'data'  =>  $data,
                'draw' => intval($request->draw),
                'recordsTotal' => $total,
                'recordsFiltered' => $total
            ];

           return response()->json($response);
       }
       return view('admin.package.index');
    }

    public function store(Request $request)
    {
        if( $request->isMethod('POST') ){
            $validator = $request->validate([
                'title'         => 'required|string|max:191',
                'paket'         => 'required|string|max:191',
                'oldprice'         => 'required|numeric',
                'currentprice'         => 'required|numeric',
                'description'   => 'required',
                'detail'        => 'nullable',                
            ]);

            $packageweb                 = new PackageWeb();
            $packageweb->title          = $request->title;
            $packageweb->paket          = $request->paket;
            $packageweb->currentprice   = $request->currentprice;
            $packageweb->oldprice       = $request->oldprice;
            $packageweb->description    = $request->description;
            $packageweb->detail         = $request->detail;
            $packageweb->save();

            foreach ($request->images as $image) {

                $filename  = $image->getClientOriginalName();
                $path      = $image->store('packageweb');
                $extension = $image->getClientOriginalExtension();
                $size      = $image->getClientSize();
                
                $image            = new Image();
                $image->filename  = time() . '_' . $filename;
                $image->title     = $request->title;
                $image->path      = $path;
                $image->extension = $extension;
                $image->size      = $size;
                $image->save();


                $packagewebimage = new PackageWebImage();           
                $packagewebimage->package_web()->associate($packageweb);
                $packagewebimage->image()->associate($image);
                $packagewebimage->save();
            }
            
            

            return response()->json([
                'success' => true,
                'message'   => 'Package Successfully Added'
            ]);
        }
        return view('admin.package.addweb');
    }

    public function update(Request $request, $id)
    {
        $packageweb = PackageWeb::find($id);
        if( $request->isMethod('POST') ){
            $validator = $request->validate([
                'title'         => 'required|string|max:191',
                'paket'         => 'required|string|max:191',
                'oldprice'         => 'required|numeric',
                'currentprice'         => 'required|numeric',
                'description'   => 'required',
                'detail'        => 'nullable', 
            ]);

            $packageweb->title          = $request->title;
            $packageweb->paket          = $request->paket;
            $packageweb->currentprice   = $request->currentprice;
            $packageweb->oldprice       = $request->oldprice;
            $packageweb->description    = $request->description;
            $packageweb->detail         = $request->detail;
        
            if($request->deleted_image != null){
                foreach ($request->deleted_image as $deleted_images) {
                    $packagewebimage = PackageWebImage::find($deleted_images);
                    Storage::delete($packagewebimage->image->path);
                    $packagewebimage->delete();
                }
            }
            
            if($request->images != null)
            {
                foreach ($request->images as $image) {

                    $filename  = $image->getClientOriginalName();
                    $path      = $image->store('packageweb');
                    $extension = $image->getClientOriginalExtension();
                    $size      = $image->getClientSize();
                    
                    $image            = new Image();
                    $image->filename  = time() . '_' . $filename;
                    $image->title     = $request->title;
                    $image->path      = $path;
                    $image->extension = $extension;
                    $image->size      = $size;
                    $image->save();


                    $packagewebimage = new PackageWebImage();            
                    $packagewebimage->package_web()->associate($packageweb);
                    $packagewebimage->image()->associate($image);
                    $packagewebimage->save();
                }     
            }

            $packageweb->save();
           

            return response()->json([
                'success' => true,
                'message'   => 'Package Successfully Updated'
            ]);
        }

        return view('admin.package.updateweb', ['packageweb' => $packageweb]);
    }

    public function destroy($id)
    {
        $web = Web::with('package_web_images.image')->find($id);
        $web_images = $web->package_web_images;
        
        foreach ($web_images as $web_image) {
            $image = Image::find( $web_image->image_id);
            Storage::delete($image->path);
            $image->delete();
        }

        $web->delete();

        return response()->json([
            'success' => true,
            'message'   => 'Package Web Successfully Deleted'
        ]);
    }
}
