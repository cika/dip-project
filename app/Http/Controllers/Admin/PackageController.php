<?php

namespace App\Http\Controllers\Admin;
use App\Model\Package;
use App\Model\Image;
use App\Model\PackageImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class PackageController extends Controller
{
    public function index(Request $request)
    {
    	  if( $request->isMethod('post') ){
            $search;
            $start = $request->start;
            $length = $request->length;

            if( !empty($request->search) )
                $search = $request->search['value'];
            else
                $search = null;

            $column = [
                "title","frame","type","price"
            ];

            $total = Package::where(function ($query) use ($search){
                        $query->where("title", 'LIKE', "%$search%")
                            ->orWhere("frame", 'LIKE', "%$search%")
                            ->orWhere("type", 'LIKE', "%$search%")
                            ->orWhere("price", 'LIKE', "%$search%");
                    })->count();
            $data = Package::where(function ($query) use ($search){
                        $query->where("title", 'LIKE', "%$search%")
                            ->orWhere("frame", 'LIKE', "%$search%")
                            ->orWhere("type", 'LIKE', "%$search%")
                            ->orWhere("price", 'LIKE', "%$search%");
                    })
                    ->orderBy($column[$request->order[0]['column'] - 1], $request->order[0]['dir'])
                    ->skip($start)
                    ->take($length)
                    ->get();

            $response = [
                'data'  =>  $data,
                'draw' => intval($request->draw),
                'recordsTotal' => $total,
                'recordsFiltered' => $total
            ];

           return response()->json($response);
       }
        return view('admin.package.index');
    }

    public function store(Request $request)
    {
        if( $request->isMethod('POST') ){
            $validator = $request->validate([
                'title'         => 'required|string|max:191',
                'frame'         => 'required|string|max:191',
                'type'          => 'nullable|string',
                'price'         => 'required|numeric',
                'unit'          => 'required',
                'description'   => 'required',
                'detail'        => 'nullable',                
            ]);

            $package              = new Package();
            $package->title       = $request->title;
            $package->frame       = $request->frame;
            $package->type        = $request->type;
            $package->price       = $request->price;
            $package->unit        = $request->unit;
            $package->description = $request->description;
            $package->detail      = $request->detail;
            $package->save();


            foreach ($request->images as $image) {

                $filename  = $image->getClientOriginalName();
                $path      = $image->store('package');
                $extension = $image->getClientOriginalExtension();
                $size      = $image->getClientSize();
                
                $image            = new Image();
                $image->filename  = time() . '_' . $filename;
                $image->title     = $request->title;
                $image->path      = $path;
                $image->extension = $extension;
                $image->size      = $size;
                $image->save();


                $packageimage = new PackageImage();            
                $packageimage->package()->associate($package);
                $packageimage->image()->associate($image);
                $packageimage->save();
            }

            return response()->json([
                'success' => true,
                'message'   => 'Package Successfully Added'
            ]);
        }
        return view('admin.package.add');
    }

    public function update(Request $request, $id)
    {
        $package = Package::find($id);
        if ($request->isMethod('POST')) 
        {
            // dd($request->all());
            $validator = $request->validate([
                    'title'         => 'required|string|max:191',
                    'frame'         => 'required|string|max:191',
                    'type'          => 'nullable|string',
                    'price'         => 'required|numeric',
                    'unit'          => 'required',
                    'description'   => 'required',
                    'detail'        => 'nullable',
            ]);

            $package->title       = $request->title;
            $package->frame       = $request->frame;
            $package->type        = $request->type;
            $package->price       = $request->price;
            $package->unit        = $request->unit;
            $package->description = $request->description;
            $package->detail      = $request->detail;

            
            if($request->deleted_image != null){
                foreach ($request->deleted_image as $deleted_images) {
                    $packageimage = PackageImage::find($deleted_images);
                    Storage::delete($packageimage->image->path);
                    $packageimage->delete();
                }
            }
            
            if($request->images != null)
            {
                foreach ($request->images as $image) {

                    $filename  = $image->getClientOriginalName();
                    $path      = $image->store('package');
                    $extension = $image->getClientOriginalExtension();
                    $size      = $image->getClientSize();

                    $image            = new Image();
                    $image->filename  = time() . '_' . $filename;
                    $image->title     = $request->title;
                    $image->path      = $path;
                    $image->extension = $extension;
                    $image->size      = $size;
                    $image->save();

                    $packageimage = new PackageImage();            
                    $packageimage->package()->associate($package);
                    $packageimage->image()->associate($image);
                    $packageimage->save();    
                }
            }
                

            $package->save();

            return response()->json([
                'success' => true,
                'message'   => 'Package Successfully Updated'
            ]);
        }
        
        return view('admin.package.update', ['package' => $package]);
    }

    public function destroy($id)
    {
        $package = Package::with('package_images.image')->find($id);

        $package_images = $package->package_images;
        
        foreach ($package_images as $package_image) {
            $image = Image::find( $package_image->image_id);
            Storage::delete($image->path);
            $image->delete();
        }

        $package->delete();

        return response()->json([
            'success' => true,
            'message'   => 'Package Successfully Deleted'
        ]);
    }


}
