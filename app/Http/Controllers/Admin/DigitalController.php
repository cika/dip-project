<?php

namespace App\Http\Controllers\Admin;
use App\Model\Digital;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\support\Facades\DB;

class DigitalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $digital = Digital::where("name", "digital")->first();
        

        if ($request->isMethod('POST')) {
            $digital->content = $request->digital;
            $digital->save();

            return response()->json([
                'success' => true,
                'message' => 'Data Successfully Changed'
            ]);

        }
        return view('admin.digital.index', ['digital' => $digital]);
    }
}
