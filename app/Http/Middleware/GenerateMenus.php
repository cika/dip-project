<?php

namespace App\Http\Middleware;

use Closure;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        if($user != null){
            \Menu::make('MyNavBar', function ($menu) use ($user) {

                $menu->add('<span>Dashboard</span>', ['route' => 'admin.home'])
                     ->prepend('<i class="fa fa-dashboard"></i>');


                $menu->add('<span>Google Analytic</span>', ['route' => 'admin.googleanalytics'])
                     ->prepend('<i class="fa fa-dashboard"></i>');

                /**
                 * Setting Site
                 */ 

                $setting = $menu->add('<span>Setting Site</span>', ['class' => 'treeview'])
                     ->append('<span class="pull-right-container"></span><i class="fa fa-angle-left pull-right"></i>')
                     ->prepend('<i class="fa fa-television"></i>');

                $setting->add('<span>Slide</span>',['route'=> 'slide.index'])
                ->prepend('<i class="fa fa-sliders"></i>');

                $setting->add('<span>Change Profile Company</span>',['route'=> 'contact.index'])
                ->prepend('<i class="fa fa-address-book"></i>');

                $setting->add('<span>About Us</span>',['route'=> 'about.index'])
                ->prepend('<i class="fa fa-info"></i>');
                
                $menu->add('<span>Data Package</span>', ['route' => 'package.index'])
                     ->prepend('<i class="fa fa-cube"></i>');

                $menu->add('<span>Digital Imaging</span>', ['route' => 'digital.index'])
                     ->prepend('<i class="fa fa-clipboard"></i>');


                $trace = $menu->add('<span>Trace Photography </span>', ['class' => 'treeview'])        
                     ->prepend('<i class="fa fa-map-marker"></i>')
                     ->append('<span class="pull-right-container"></span><i class="fa fa-angle-left pull-right"></i>');

                $trace->add('<span>Tracking</span>', ['route' => 'tracking.index'])
                     ->prepend('<i class="fa fa-location-arrow"></i>');

                $trace->add('<span>Cancel Tracking</span>', ['route' => 'tracking.cancel'])
                     ->prepend('<i class="fa fa-thumb-tack"></i>');
                    

            });


        }

        return $next($request);
    }
}
