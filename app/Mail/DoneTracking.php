<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Model\Tracking;

class DoneTracking extends Mailable
{
    use Queueable, SerializesModels;
    public $tracking;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($tracking)
    {
        $this->tracking = $tracking;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->replyTo($this->tracking->email)
            ->view('emails.done');
    }
}
