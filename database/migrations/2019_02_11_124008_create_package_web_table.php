<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageWebTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_web', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('paket');
            $table->text('description');
            $table->text('detail');
            $table->integer('oldprice');
            $table->integer('currentprice');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_web');
    }
}
