<?php
use App\Model\Contact;
use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	if(Contact::where('email','contact@dipproject.id')->first() === null){
	        $contact 			= new Contact();
	        $contact->address 	= 'Jl. M.H Thamrin , Sei Rengas I, Medan Kota, Medan, Sumatera Utara, 20211';
	        $contact->phone 	= '+6287766116931';
	        $contact->email 	= 'contact@dipproject.id';
	        $contact->web 		= 'http://dip-project.com';
	        $contact->save();
    	}
    }
}
