<?php
use App\Model\Digital;
use Illuminate\Database\Seeder;

class DigitalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
  	    if (!DB::table('digitals')->where('name' ,'=', 'digital')->first()) {
            DB::table('digitals')->insert([
                'name' => 'digital',
                'content' => '<h1 style="text-align:center"><span style="font-size:18px"><strong><span style="color:#f1c40f">DIGITAL IMAGING PACKAGES</span></strong></span></h1>

<p>&nbsp;</p>

<p>Photoshoot is not just enough! &nbsp;You need to optimize the result. &nbsp;Let us help you to scale up your image quality<br />
and of course it will help you to scale up you SALES!</p>

<table border="1" cellpadding="1" cellspacing="1" style="width:500px">
    <tbody>
        <tr>
            <td>No</td>
            <td>Type Of Task</td>
            <td>Rate / Image (Rp)</td>
        </tr>
        <tr>
            <td>1.</td>
            <td>Remove Background Standard</td>
            <td>6,750</td>
        </tr>
        <tr>
            <td>2.</td>
            <td>Remove Background Complex</td>
            <td>27,000</td>
        </tr>
        <tr>
            <td>3.</td>
            <td>Background Replacement</td>
            <td>8,100</td>
        </tr>
        <tr>
            <td>4.</td>
            <td>Ghost Mannequin</td>
            <td>12,000</td>
        </tr>
        <tr>
            <td>5.</td>
            <td>Flatlay</td>
            <td>11,250</td>
        </tr>
        <tr>
            <td>6.</td>
            <td>Cropping</td>
            <td>1,200</td>
        </tr>
        <tr>
            <td>7.</td>
            <td>Major Retouching</td>
            <td>6,750</td>
        </tr>
        <tr>
            <td>8.</td>
            <td>Minor Retouching</td>
            <td>4,050</td>
        </tr>
        <tr>
            <td>9.</td>
            <td>Color Adjustment</td>
            <td>3,375</td>
        </tr>
        <tr>
            <td>10.</td>
            <td>Color Match</td>
            <td>6,750</td>
        </tr>
        <tr>
            <td>11.</td>
            <td>Shadowing</td>
            <td>2,700</td>
        </tr>
        <tr>
            <td>12.</td>
            <td>Logo Placement</td>
            <td>675</td>
        </tr>
        <tr>
            <td>13.</td>
            <td>Color Swatches</td>
            <td>675</td>
        </tr>
        <tr>
            <td>14.</td>
            <td>Color Replacement</td>
            <td>675</td>
        </tr>
        <tr>
            <td>15.</td>
            <td>Customize Edit Type</td>
            <td>Price By Discussion</td>
        </tr>
    </tbody>
</table>

<p>Price in Rupiah / IDR<br />
Price applicable for 1 &ndash;5000 images<br />
2 days working time<br />
Get Discount 10% for order above 5000 images Monthly<br />
Also we will inform you if there are any products which should be in complex category.</p>

<p>Complex images such as :<br />
-Necklace<br />
-Hairy Product<br />
-Products with a lot of holes<br />
-Chandelier<br />
-etc&nbsp;</p>'
            ]);
        }
    }
}
