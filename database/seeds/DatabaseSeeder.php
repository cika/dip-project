<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminsTableSeeder::class);
        $this->call(DigitalsTableSeeder::class);
        $this->call(ContactsTableSeeder::class);
        $this->call(AboutsTableSeeder::class);
    }
}
