<?php
use App\Model\AboutUs;
use Illuminate\Database\Seeder;

class AboutsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AboutUs::firstOrCreate(['name' => 'line1'],['content' => 'lorem']);
        AboutUs::firstOrCreate(['name' => 'line2'],['content' => 'lorem lorem']);
        AboutUs::firstOrCreate(['name' => 'line3'],['content' =>  'lorem lorem lorem']);
    }
}
